export default {
  items: [
    {
      name: 'IM Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
      // badge: {
      // variant: 'primary',
      // text: 'NEW'
      // }
    },
    {
      title: true,
      name: 'Knowledge'
    },
    {
      name: 'Browse worldview',
      url: '/components/buttons',
      icon: 'icon-puzzle'
    },
    {
      name: 'Models and data',
      url: '/components/social-buttons',
      icon: 'icon-puzzle'
    },
    {
      name: 'Observations',
      url: '/components/cards',
      icon: 'icon-puzzle'
    },
    {
      divider: true
    },
    {
      title: true,
      name: 'Assets'
    },
    {
      name: 'Manage',
      url: '/icons/font-awesome',
      icon: 'icon-star'
    },
    {
      name: 'Contribute',
      url: '/widgets',
      icon: 'icon-calculator'
      // badge: {
      // variant: 'danger',
      // text: 'NEW'
      // }
    },
    {
      name: 'Query and browse',
      url: '/icons/simple-line-icons',
      icon: 'icon-star'
    },
    {
      name: 'Usage',
      url: '/icons/simple-line-icons',
      icon: 'icon-star'
    },
    {
      divider: true
    },
    {
      title: true,
      name: 'Network'
    },
    {
      name: 'Browse',
      url: '/icons/font-awesome',
      icon: 'icon-star'
    },
    {
      name: 'Query',
      url: '/icons/simple-line-icons',
      icon: 'icon-star'
    },
    {
      divider: true
    },
    {
      title: true,
      name: 'Account'
    },
    {
      name: 'Edit details',
      url: '/pages/login',
      icon: 'icon-star'
    },
    {
      name: 'Renew certificate',
      url: '/pages/register',
      icon: 'icon-star'
    },
    {
      name: 'Change worldview',
      url: '/pages/login',
      icon: 'icon-star'
    },
    {
      name: 'Cancel account',
      url: '/pages/404',
      icon: 'icon-star'
    }
  ]
}
