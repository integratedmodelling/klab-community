import {endpoints, url} from '../rest'
import * as types from '../store/mutation-types'
import {serverError} from '../notifier'

export default {

  login: function (context, credentials) {
    context.$http.post(url(endpoints.AUTHENTICATION_USERNAME), credentials).then(response => {
      if (!serverError(response.body, context)) {
        context.$store.commit(types.AUTHORIZE_USER, response.body)
        context.$router.push('/')
      }
    }, response => {
      serverError(response.body, context)
    })
  }
  // signup (creds) {
  //   context.$http.post(url(endpoints.AUTHENTICATION_REGISTER), creds, (data) => {
  //     localStorage.setItem('id_token', data.id_token)
  //
  //     this.user.authenticated = true
  //
  //     if (redirect) {
  //       router.go(redirect)
  //     }
  //   }).error((err) => {
  //     context.error = err
  //   })
  // },
  //
  // logout () {
  //   localStorage.removeItem('id_token')
  //   this.user.authenticated = false
  // }
}
