import * as Cookies from 'js-cookie'

const COOKIE_KEY = 'imdashboard'
const EXPIRATION_DAYS = 7

var cookie = null
var done = false

function getCookie () {
  if (!done && !cookie) {
    cookie = Cookies.getJSON(COOKIE_KEY)
    alert('Cookie is ' + JSON.stringify(cookie, null, 2))
  }
  done = true
  return cookie
}

export function getToken () {
  return getCookie() == null ? null : cookie.token
}

export function getUsername () {
  return getCookie() == null ? null : cookie.username
}

export function setCookie (userData) {
  var dio = Cookies.set(COOKIE_KEY, { token: userData.authToken, username: userData.username }, { expires: EXPIRATION_DAYS, secure: true })
  alert('Cookie set ' + dio)
}
