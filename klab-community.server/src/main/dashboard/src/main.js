// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import BootstrapVue from 'bootstrap-vue'
import Notifications from 'vue-notification'
import App from './App'
import router from './router'
import store from './store'

Vue.use(Notifications)
Vue.use(BootstrapVue)
Vue.use(VueResource)

var unsecured = ['Login', 'Register']

/**
 * Any URL except the unsecured components above redirects to
 * the login page unless we're authenticated.
 */
router.beforeEach((to, from, next) => {
  if (!store.getters.isAuthenticated && !unsecured.includes(to.name)) {
    next('/pages/login')
  } else {
    next()
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: {
    App
  }
})
