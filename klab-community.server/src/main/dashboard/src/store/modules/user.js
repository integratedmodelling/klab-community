import * as types from '../mutation-types'
import * as cookies from '../../cookies'

const state = {
  token: cookies.getToken(),
  username: cookies.getUsername()
}

const getters = {
  isAuthenticated: function (state) { return state.token !== null },
  getUsername: function (state) { return state.username }
}

const actions = {
  authorize: function ({commit, state}, userProfile) {
    commit(types.AUTHORIZE_USER)
  }
}

const mutations = {
  [types.AUTHORIZE_USER] (state, userProfile) {
    state.token = userProfile.authToken
    state.username = userProfile.username
    cookies.setCookie(userProfile)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
