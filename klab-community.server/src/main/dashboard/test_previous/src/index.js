import Vue from 'vue'
import App from './components/App.vue'
import Home from './components/Home.vue'
import SecretQuote from './components/SecretQuote.vue'
import Signup from './components/Signup.vue'
import Login from './components/Login.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.use(VueRouter)
import auth from './auth'

// Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');

export var endpoints = {
    AUTHENTICATION_UP: '/authentication',
    AUTHENTICATION_CERT: '/authentication/cert-file',
    AUTHENTICATION_REGISTER: '/profile' /* with POST - TODO I see the point but maybe change to more expressive name? */
}
export var host = "http://localhost:8151";

export function url(endpoint) {
    return host.concat(endpoint);
}

// Check the user's auth status when the app starts
auth.checkAuth()

export var router = new VueRouter()

router.map({
  '/home': {
    component: Home
  },
  'secretquote': {
    component: SecretQuote
  },
  '/login': {
    component: Login
  },
  '/signup': {
    component: Signup
  }
})

router.redirect({
  '*': '/home'
})

router.start(App, '#app')
