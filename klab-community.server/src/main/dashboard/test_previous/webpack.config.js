module.exports = {
  // the main entry of our app
  entry: ['./src/index.js', './src/auth/index.js'],
  // output configuration
  output: {
    path: __dirname + '/build/',
    publicPath: 'build/',
    filename: 'build.js'
  },

  module: {
    loaders: [
      // process *.vue files using vue-loader
    { 
    	  test: /\.vue$/, 
    	  loader: 'vue-loader' 
      },
      // process *.js files using babel-loader
      // the exclude pattern is important so that we don't
      // apply babel transform to all the dependencies!
      { 
    	  test: /\.js$/, 
    	  exclude: /node_modules/, 
		  loader: 'babel-loader', 
//    	  use: {
//    		  options: {
//    			  presets: ['es2015'],
//    			  plugins: ['transform-runtime']
//    		  } 
//    	  } 
      }
    ]
  }

}