// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.resource.validation;

public interface Constraints {

    /**
     * allows alphanumeric, dot-separated usernames of up to 24 characters per "word".
     */
    String USERNAME_PATTERN = "^[A-Za-z0-9]{3,24}((\\.)[A-Za-z0-9]{2,24})*$";

    String DATASOURCE_NAME_PATTERN = "^[-_A-Za-z0-9]*$";

    // NOTE: don't use ^...$ because it needs to be inserted into another regex
    String POSTGRESQL_TABLE_NAME_PATTERN = "[_A-Za-z0-9]+";

    /**
     * These are to be used in *cleansing* rather than *validation*. We should silently rename,
     * rather than complaining to the user about stuff they don't care about (and we should definitely
     * not tell them to rename their files).
     */
    String FILENAME_ILLEGAL_CHARACTERS = "[^-_.A-Za-z0-9]";

    int USERNAME_LENGTH = 50;

}
