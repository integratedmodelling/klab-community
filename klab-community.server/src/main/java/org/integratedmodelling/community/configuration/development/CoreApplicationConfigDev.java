// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration.development;

import java.net.MalformedURLException;
import java.net.URL;

import org.integratedmodelling.community.configuration.CoreApplicationConfig;
import org.integratedmodelling.community.exceptions.CorruptOrBadDataException;
import org.springframework.stereotype.Component;

@Component
public class CoreApplicationConfigDev extends CoreApplicationConfig {

    public static final String DEV_SERVER_HTTP_PORT = "8390";

    @Override
    public String getSiteName() {
        return "Integrated Modelling Dashboard (dev server)";
    }

    @Override
    public String getSiteUniqueShortname() {
        return "im-dashboard-dev";
    }

    @Override
    public String getSiteServerId() {
        return "im-dev";
    }

    @Override
    public URL getSiteUrl() {
        try {
            return new URL("http://localhost:8390");
        } catch (MalformedURLException e) {
            String msg = "Unable to create site URL! (check the dev config string.)";
            logger.error(msg, e);
            throw new CorruptOrBadDataException(msg, e);
        }
    }

    @Override
    public URL getEngineUrl() {
        try {
            return new URL("http://localhost:8390/tl0");
        } catch (MalformedURLException e) {
            String msg = "Unable to create engine URL! (check the dev config string.)";
            logger.error(msg, e);
            throw new CorruptOrBadDataException(msg, e);
        }
    }

    // gpg --print-mds src/main/resources/keyring/secring-dev.gpg
    private static final String CERT_FILE_DIGEST_SHA512 = "A1A09612 FC582375 E4FCE090 62293FB9 "
            + "0F656CF0 08773A61 65380332 806EDAF0 C2866AB1 BD29AC0A E67F1298 15326A9E "
            + "B982D16F 8600475F AE731C99 CFE822FB";

    // gpg --print-mds src/main/resources/keyring/pubring-dev.gpg
    private static final String CERT_FILE_DIGEST_SHA512_PUBLICKEY = "EA883B10 E48BF477 4AE96ED8 "
            + "469AAC51 92045AF1 B428EC56 6AFEEF8A 65012C73 D533CEA9 510AD15A 42BE067D "
            + "8C9CC3F8 9A919C5E F7F1C3D1 C99212FD 327C2751";

    @Override
    public String getKeyringFile() {
        return "src/main/resources/keyring/secring-dev.gpg";
    }

    @Override
    public String getKeyringPublicKeyFile() {
        return "src/main/resources/keyring/pubring-dev.gpg";
    }

    @Override
    public String getKeyringUserId() {
        return "Dev Server <dev.server@localhost>";
    }

    @Override
    public String getKeyringPassphrase() {
        return "Dev Server";
    }

    @Override
    public byte[] getKeyringDigest() {
        return decodeHex(CERT_FILE_DIGEST_SHA512);
    }

    @Override
    public byte[] getKeyringPublicKeyDigest() {
        return decodeHex(CERT_FILE_DIGEST_SHA512_PUBLICKEY);
    }
}
