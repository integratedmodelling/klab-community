// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration.development;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.configuration.CoreApplicationConfig;
import org.integratedmodelling.community.configuration.WebSecurityConfig;
import org.integratedmodelling.community.security.KLabUserDetailsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Component;

/**
 * This class provides runtime decoupling from the underlying UserDetailsManager.
 * Decoupling allows this class to load as a bean with the standard Spring Bean mechanism,
 * so that the LDAP sibling class can load settings from .properties file(s)
 *
 * this test class contains sensible test accounts that can be remembered, demo'd, shared publicly, etc.
 */
@Component
public class KLabUserDetailsManagerDev extends KLabUserDetailsManager {

    private InMemoryUserDetailsManager wrapped;

    @Autowired
    private CoreApplicationConfig appConfig;

    @Autowired
    private WebSecurityConfig securityConfig;

    @PostConstruct
    public void postConstruct() throws Exception {
        List<UserData> initialUsers = securityConfig.getInitialUsers();
        // this loop forces the DetailsManager to not use delegate objects
        List<UserDetails> initialUserData = new ArrayList<>(initialUsers.size());
        for (UserData user : initialUsers) {
            initialUserData.add(user);
        }
        wrapped = new InMemoryUserDetailsManager(initialUserData);
    }

    @Override
    protected InMemoryUserDetailsManager getWrappedObject() {
        return wrapped;
    }
}
