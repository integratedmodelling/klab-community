// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration;

import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;

@EnableMongoRepositories
@EnableMongoAuditing
// @Component is not valid here because it needs to be on the concrete class
public abstract class MongoConfig extends AbstractMongoConfiguration {

    @Override
    protected String getMappingBasePackage() {
        return "org.integratedmodelling";
    }

    @Override
    public MongoClient mongo() throws Exception {
        return new MongoClient(getHost(), getPort());
    }

    protected String getHost() {
        return "localhost";
    }

    protected int getPort() {
        return 27017;
    }

    //    // something like this.
    //    @Bean
    //    public <T> CustomMongoRepository<T> buildCustomRepositoryBean(Class<T> clazz) throws Exception {
    //        return new CustomMongoRepository<T>(clazz, mongo(), getAuthDatabaseName());
    //    }

    protected abstract String getAuthDatabaseName();
}
