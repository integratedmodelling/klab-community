// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration.development;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.integratedmodelling.community.beans.Role;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.beans.UserData.AccountStatus;
import org.integratedmodelling.community.configuration.WebSecurityConfig;
import org.integratedmodelling.community.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * LDAP-mimicking config for integration tests and for standing up a local instance on your workstation.
 *
 * @author luke
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfigDev extends WebSecurityConfig { // which extends WebSecurityConfigurerAdapter

    @Autowired
    private KLabUserDetailsManagerDev userDetailsManager;

    @Autowired
    private UserRepository userRepository;

    public static final UserData system = testUser("system", "password", "admin@integratedmodelling.org", "Joe",
            "Robot", Role.ROLE_USER, Role.ROLE_ADMINISTRATOR, Role.ROLE_SYSTEM);

    public static final UserData hades = testUser("hades", "password", "hades@integratedmodelling.org", "Hades",
            "of Greece", Role.ROLE_USER, Role.ROLE_ADMINISTRATOR);

    public static final UserData hercules = testUser("hercules", "password", "hercules@integratedmodelling.org",
            "Hercules", "of Rome", Role.ROLE_USER);

    /**
     * Achilles will exist in Mongo but not LDAP, simulating a "black hole" scenario - a registered user that is
     * considered "active" but can't log in.
     *
     * This state should be fixed by doing a password reset ("Forgot Password").
     */
    public static final UserData achilles_activeMissingLdap = testUser("achilles", "password",
            "achilles@integratedmodelling.org", "Achilles", "of Greece", Role.ROLE_USER);

    /**
     * Triton simulates another black hole scenario - a registration where the email is lost,
     * so the user has a Mongo account (in 'pending' state) but no LDAP record.
     *
     * This state should be solved by re-registering.
     */
    public static final UserData triton_pendingMissingLdap = testUser("triton", "password",
            "triton@integratedmodelling.org", "Triton", "of Greece", Role.ROLE_USER);

    static {
        system.addGroups("ARIESTEAM");
        system.addGroups("ASSETS");
        system.addGroups("ISU");
        hades.addGroups("ARIESTEAM");
        hades.addGroups("ISU");
        hercules.addGroups("ISU");
        achilles_activeMissingLdap.addGroups("ARIESTEAM");
        triton_pendingMissingLdap.addGroups("ARIESTEAM");
        triton_pendingMissingLdap.setAccountStatus(AccountStatus.pendingActivation);
    }

    @Override
    public void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
        logger.info("Configuring TEST security settings");
        authBuilder.userDetailsService(userDetailsManager);
    }

    private static UserData testUser(String username, String password, String email, String firstName, String lastName,
            Role... roles) {
        UserData result = new UserData();
        result.setUsername(username);
        result.setPasswordHash(password);
        result.setEmail(email);
        result.setRoles(Arrays.asList(roles));
        result.setAccountStatus(AccountStatus.active);
        result.setFirstName(firstName);
        result.setLastName(lastName);
        result.setRegistrationDate();
        return result;
    }

    @Override
    public List<UserData> getInitialUsers() {
        return new ArrayList<>(Arrays.asList(hercules, hades, system));
    }
}
