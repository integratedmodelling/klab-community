// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration.production;

import java.util.Properties;

import org.integratedmodelling.community.configuration.EmailConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class EmailConfigProd extends EmailConfig {

    @Value("${email.server.hostname}")
    private String EMAIL_HOSTNAME;

    @Value("${email.server.port:25}")
    private int EMAIL_PORT;

    @Value("${email.server.username}")
    private String EMAIL_USERNAME;

    @Value("${email.server.password}")
    private String EMAIL_PASSWORD;

    @Value("${email.replyable.general.emailaddress}")
    private String EMAIL_REPLYABLE_GENERAL;

    @Value("${email.replyable.admin.emailaddress}")
    private String EMAIL_REPLYABLE_ADMIN;

    @Value("${email.noreply.emailaddress}")
    private String EMAIL_NOREPLY;

    @Override
    public JavaMailSender getEmailSender() {
        JavaMailSenderImpl result = new JavaMailSenderImpl();
        result.setHost(EMAIL_HOSTNAME);
        result.setPort(EMAIL_PORT);
        result.setUsername(EMAIL_USERNAME);
        result.setPassword(EMAIL_PASSWORD);
        Properties javaMailProperties = new Properties();
        javaMailProperties.setProperty("mail.smtp.auth", "true");
        javaMailProperties.setProperty("mail.smtp.starttls.enable", "true");
        result.setJavaMailProperties(javaMailProperties);
        return result;
    }

    @Override
    public String replyableGeneralEmailAddress() {
        return EMAIL_REPLYABLE_GENERAL;
    }

    @Override
    public String replyableAdminEmailAddress() {
        return EMAIL_REPLYABLE_ADMIN;
    }

    @Override
    public String noreplyEmailAddress() {
        return EMAIL_NOREPLY;
    }
}
