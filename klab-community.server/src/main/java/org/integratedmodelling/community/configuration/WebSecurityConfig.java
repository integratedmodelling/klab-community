// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.integratedmodelling.community.KLabLogger;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.exceptions.AuthenticationFailedException;
import org.integratedmodelling.community.security.KLabUserDetailsManager;
import org.integratedmodelling.community.security.TokenHeaderProcessingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

// @Component is not valid here because it needs to be on the concrete class

// @EnableGlobalMethodSecurity sets up processors for authorization advice
// that can be added around methods and classes. This authorization advice
// lets a developer write Spring EL that inspects input parameters and return types.
@EnableGlobalMethodSecurity(securedEnabled = true)
public abstract class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String AUTHENTICATION_TOKEN_HEADER_NAME = "Authentication";

    protected final KLabLogger logger = new KLabLogger(getClass());

    @Autowired
    KLabUserDetailsManager userDetailsManager;

    @Autowired
    TokenHeaderProcessingFilter tokenHeaderProcessingFilter;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        logger.info("HTTP config: disabling session management...");
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
        http.csrf().disable();

        logger.info("HTTP config: configuring token header authentication filter...");
        http.addFilterBefore(tokenHeaderProcessingFilter, UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * this is necessary so that AuthenticationManager injection points can be populated.
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public RequestMappingHandlerMapping buildRequestMappingHandlerMappingBean() {
        RequestMappingHandlerMapping mapping = new RequestMappingHandlerMapping();

        // TODO wtf? this should prevent the URN path parameter from being gutted:
        //      /engine/datasource/im:user:dot.separated.name.space:MyDataSet
        // becomes:
        //      /engine/datasource/im:user:dot.separated.name
        mapping.setUseSuffixPatternMatch(false);

        return mapping;
    }

    /**
     * TODO incorporate this where?
     *
     * this allows us to assign our own HTTP response status code, by using an
     * exception class which is annotated with it directly. (otherwise we would
     * rely on Spring's container logic to assign a 'redirect' or other default
     * status.)
     */
    AuthenticationFailureHandler authenticationFailureHandler = new AuthenticationFailureHandler() {

        @Override
        public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                AuthenticationException e) throws IOException, ServletException {
            throw new AuthenticationFailedException("Authentication failed.");
        }
    };

    public abstract List<UserData> getInitialUsers();
}
