// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration.production;

import java.net.MalformedURLException;
import java.net.URL;

import org.integratedmodelling.community.configuration.CoreApplicationConfig;
import org.integratedmodelling.community.exceptions.CorruptOrBadDataException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CoreApplicationConfigProd extends CoreApplicationConfig {

    @Value("${site.fullname}")
    private String SITE_FULLNAME;

    @Value("${site.shortname}")
    private String SITE_SHORTNAME;

    @Value("${site.url}")
    private String SITE_URL;

    @Value("${site.server.id}")
    private String SITE_SERVER_ID;

    @Value("${engine.url}")
    private String ENGINE_URL;

    @Override
    public String getSiteName() {
        return SITE_FULLNAME;
    }

    @Override
    public String getSiteUniqueShortname() {
        return SITE_SHORTNAME;
    }

    @Override
    public String getSiteServerId() {
        return SITE_SERVER_ID;
    }

    @Override
    public URL getSiteUrl() {
        try {
            return new URL(SITE_URL);
        } catch (MalformedURLException e) {
            String msg = String.format(
                    "Unable to create site URL for '%s'! (there is probably a configuration problem.)", SITE_URL);
            logger.error(msg, e);
            throw new CorruptOrBadDataException(msg, e);
        }
    }

    @Override
    public URL getEngineUrl() {
        try {
            return new URL(ENGINE_URL);
        } catch (MalformedURLException e) {
            String msg = String.format(
                    "Unable to create engine URL for '%s'! (there is probably a configuration problem.)", ENGINE_URL);
            logger.error(msg, e);
            throw new CorruptOrBadDataException(msg, e);
        }
    }
    
    @Value("${auth.keyring.filename}")
    private String KEYRING_FILENAME;

    @Value("${auth.keyring.filename.publickey}")
    private String KEYRING_FILENAME_PUBLICKEY;

    @Value("${auth.keyring.digest}")
    private String KEYRING_DIGEST;

    @Value("${auth.keyring.digest.publickey}")
    private String KEYRING_DIGEST_PUBLICKEY;

    @Value("${auth.keyring.userid}")
    private String KEYRING_USER_ID;

    @Value("${auth.keyring.passphrase}")
    private String KEYRING_PASSPHRASE;

    @Override
    public String getKeyringFile() {
        return KEYRING_FILENAME;
    }

    @Override
    public String getKeyringPublicKeyFile() {
        return KEYRING_FILENAME_PUBLICKEY;
    }

    @Override
    public byte[] getKeyringDigest() {
        return decodeHex(KEYRING_DIGEST);
    }

    @Override
    public byte[] getKeyringPublicKeyDigest() {
        return decodeHex(KEYRING_DIGEST_PUBLICKEY);
    }

    @Override
    public String getKeyringUserId() {
        return KEYRING_USER_ID;
    }

    @Override
    public String getKeyringPassphrase() {
        return KEYRING_PASSPHRASE;
    }
}
