// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.integratedmodelling.community.KLabLogger;
import org.integratedmodelling.community.beans.tokens.ClickbackAction;
import org.integratedmodelling.community.exceptions.CorruptOrBadDataException;

// @Component is not valid here because it needs to be on the concrete class
public abstract class CoreApplicationConfig {

    protected final KLabLogger logger = new KLabLogger(getClass());

    /**
     * The plain-language name of your site (and Node instance), which will go in most titles, etc.
     */
    public abstract String getSiteName();

    public abstract URL getSiteUrl();

    public abstract URL getEngineUrl();

    public URL getClientClickbackUrl(ClickbackAction action, String token) {
        try {
            // ex: http://integratedmodelling.org/collaboration/#/clickback/activate/df59b4c2-ab66-4ad0-905d-e2afe7542021
            return new URL(String.format("%s/#/clickback/%s/%s", getSiteUrl(), action, token));
        } catch (MalformedURLException e) {
            String msg = "Unable to create clickback URL! (there is probably a configuration problem.)";
            logger.error(msg, e);
            throw new CorruptOrBadDataException(msg, e);
        }
    }

    public abstract String getSiteUniqueShortname();

    public String getVersion() {
        return "0.1.0";
    }

    public final String getApplicationId() {
        return getSiteUniqueShortname() + "-" + getVersion();
    }

    public abstract String getSiteServerId();
    
    public static final String CERT_FILE_NAME = "im.cert";

    public abstract String getKeyringFile();

    public abstract String getKeyringPublicKeyFile();

    public abstract String getKeyringUserId();

    public abstract String getKeyringPassphrase();

    public abstract byte[] getKeyringDigest();

    public abstract byte[] getKeyringPublicKeyDigest();

    protected byte[] decodeHex(String digest) {
        try {
            return Hex.decodeHex(digest.replaceAll(" ", "").toCharArray());
        } catch (DecoderException e) {
            String msg = "Unable to decode hex-string digest in Dev config class";
            logger.error(msg, e);
            throw new CorruptOrBadDataException(msg);
        }
    }
}
