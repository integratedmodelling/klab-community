// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration.production;

import java.util.List;

import org.integratedmodelling.community.beans.Role;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.configuration.WebSecurityConfig;
import org.integratedmodelling.community.fixture.UserFixtureCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebSecurity
public class WebSecurityConfigProd extends WebSecurityConfig {

    @Value("${auth.ldap.groupSearchBase}")
    private String LDAP_GROUP_SEARCH_BASE;

    @Value("${auth.ldap.userIdProperty:uid}")
    private String LDAP_USER_ID_PROPERTY;

    @Value("${auth.ldap.userSearchBase:ou=imusers}")
    private String LDAP_USER_SEARCH_BASE;

    @Autowired
    private LdapContextSource contextSource;

    @Autowired
    private KLabUserDetailsManagerProd userDetailsManager;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
        logger.info("Configuring PRODUCTION security settings");

        authBuilder.ldapAuthentication().contextSource(contextSource).userSearchBase(LDAP_USER_SEARCH_BASE)
                .userSearchFilter(LDAP_USER_ID_PROPERTY + "={0}").groupSearchBase(LDAP_GROUP_SEARCH_BASE);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        // TODO production sites should be fully HTTPS.
        // this will not be necessary for arrangements with their own forwarding server
        // which can act as a SSL terminator. In this case, traffic may be sent within the datacenter
        // in the clear, if that level of security is sufficient.
        // http.requiresChannel().anyRequest().requiresSecure();
    }

    @Override
    public List<UserData> getInitialUsers() {
        List<UserData> result = new UserFixtureCollection("users.json", objectMapper, true).getAll();

        for (UserData user : result) {
            user.addRoles(Role.ROLE_USER); // so we don't force .json files to contain this
        }

        return result;
    }
}
