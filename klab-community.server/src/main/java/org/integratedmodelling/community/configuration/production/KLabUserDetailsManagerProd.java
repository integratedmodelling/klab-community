// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.configuration.production;

import javax.annotation.PostConstruct;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;
import org.integratedmodelling.community.beans.ModifyPasswordExtendedRequest;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.security.KLabUserDetailsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.ContextExecutor;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.ldap.DefaultLdapUsernameToDnMapper;
import org.springframework.security.ldap.userdetails.InetOrgPerson;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetailsManager;
import org.springframework.stereotype.Component;

/**
 * This class provides runtime decoupling from the underlying LdapUserDetailsManager.
 * Decoupling allows this class to load as a bean with the standard Spring Bean mechanism,
 * and so we can provide settings in application.properties, loaded via '@Value'
 */
@Component
public class KLabUserDetailsManagerProd extends KLabUserDetailsManager {

    @Value("${auth.ldap.groupSearchBase}")
    private String LDAP_GROUP_SEARCH_BASE;

    @Value("${auth.ldap.userIdProperty:uid}")
    private String LDAP_USER_ID_PROPERTY;

    @Value("${auth.ldap.userSearchBase}")
    private String LDAP_USER_SEARCH_BASE;

    @Autowired
    private LdapContextSource ldap;

    @Autowired
    private AuthenticationManager authenticationManager;

    private DefaultLdapUsernameToDnMapper usernameMapper;

    private LdapUserDetailsManager wrapped;

    @PostConstruct
    public void postConstruct() throws Exception {
        wrapped = new LdapUserDetailsManager(ldap);
        usernameMapper = new DefaultLdapUsernameToDnMapper(LDAP_USER_SEARCH_BASE, LDAP_USER_ID_PROPERTY);
        wrapped.setUsernameMapper(usernameMapper);
        wrapped.setGroupSearchBase(LDAP_GROUP_SEARCH_BASE);
    }

    @Override
    protected LdapUserDetailsManager getWrappedObject() {
        return wrapped;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void createSpringUser(UserData user) {
        LdapUserDetails userDetails = convertToLdapUserDetails(user);
        createUser(userDetails);
    }

    private LdapUserDetails convertToLdapUserDetails(UserData user) {
        InetOrgPerson.Essence person = new InetOrgPerson.Essence();
        person.setCn(new String[] { user.getUsername() });
        person.setDn(usernameMapper.buildDn(user.getUsername()));
        person.setUid(user.getUsername());
        person.setUsername(user.getUsername());
        person.setPassword(user.getPassword());
        person.setMail(user.getEmail());
        person.setDisplayName(user.getDisplayName());
        if (StringUtils.isEmpty(user.getLastName())) {
            person.setSn("<unknown>");
        } else {
            person.setSn(user.getLastName());
        }
        person.setAuthorities(user.getAuthorities());
        LdapUserDetails userDetails = person.createUserDetails();
        return userDetails;
    }

    @Override
    public void updateSpringUser(UserData userData) {
        LdapUserDetails userDetails = convertToLdapUserDetails(userData);
        getWrappedObject().updateUser(userDetails);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) throws AuthenticationException {
        reAuthenticateCurrentUser(oldPassword);

        DistinguishedName baseLdapPath = ldap.getBaseLdapPath();
        DistinguishedName userDn = usernameMapper.buildDn(securityHelper.getLoggedInUsername());
        String dn = String.format("%s,%s", userDn, baseLdapPath);
        final ModifyPasswordExtendedRequest extendedOperationRequest = new ModifyPasswordExtendedRequest(dn,
                newPassword);

        LdapTemplate ldapTemplate = new LdapTemplate(ldap);
        ldapTemplate.executeReadWrite(new ContextExecutor() {

            @Override
            public Object executeWithContext(DirContext ctx) throws NamingException {
                if (!(ctx instanceof LdapContext)) {
                    throw new IllegalArgumentException(
                            "Extended operations require LDAPv3 - Context must be of type LdapContext");
                }
                LdapContext ldapContext = (LdapContext) ctx;
                return ldapContext.extendedOperation(extendedOperationRequest);
            }
        });
    }

    /**
     * @throws AuthenticationException if oldPassword is not null and doesn't match existing
     */
    private void reAuthenticateCurrentUser(String oldPassword) throws AuthenticationException {
        if (oldPassword != null) {
            Authentication authRequest = new UsernamePasswordAuthenticationToken(securityHelper.getLoggedInUsername(),
                    oldPassword);
            authenticationManager.authenticate(authRequest);
        }
    }
}
