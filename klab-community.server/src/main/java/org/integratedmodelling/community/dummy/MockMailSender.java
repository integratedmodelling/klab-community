// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.dummy;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.MimeMessage;

import org.integratedmodelling.community.KLabLogger;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;

public class MockMailSender implements JavaMailSender {

    private final KLabLogger logger = new KLabLogger(getClass());

    private List<SimpleMailMessage> sentMessagesSimple = new ArrayList<>();

    private List<MimeMessage> sentMessagesMime = new ArrayList<>();

    @Override
    public void send(SimpleMailMessage simpleMessage) throws MailException {
        logger.info("- called send() with simpleMessage: " + simpleMessage.toString());
        sentMessagesSimple.add(simpleMessage);
    }

    @Override
    public void send(SimpleMailMessage[] simpleMessages) throws MailException {
        logger.info("- called send() with array of SimpleMailMessage[]...");
        for (SimpleMailMessage simpleMessage : simpleMessages) {
            send(simpleMessage);
        }
    }

    @Override
    public MimeMessage createMimeMessage() {
        logger.info("- called createMimeMessage()");
        return null;
    }

    @Override
    public MimeMessage createMimeMessage(InputStream contentStream) throws MailException {
        logger.info("- called createMimeMessage(InputStream)");
        return null;
    }

    @Override
    public void send(MimeMessage mimeMessage) throws MailException {
        logger.info("- called send(MimeMessage)");
        sentMessagesMime.add(mimeMessage);
    }

    @Override
    public void send(MimeMessage[] mimeMessages) throws MailException {
        logger.info("- called send(MimeMessage[])");
    }

    @Override
    public void send(MimeMessagePreparator mimeMessagePreparator) throws MailException {
        logger.info("- called send(MimeMessagePreparator)");
    }

    @Override
    public void send(MimeMessagePreparator[] mimeMessagePreparators) throws MailException {
        logger.info("- called send(MimeMessagePreparator[])");
    }

    public List<SimpleMailMessage> getSentMessagesSimple() {
        return sentMessagesSimple;
    }

    public List<MimeMessage> getSentMessagesMime() {
        return sentMessagesMime;
    }

    /**
     * retrieve the last-sent "simple" mail message (no mime type)
     */
    public SimpleMailMessage popSimple() {
        int i = sentMessagesSimple.size() - 1;
        SimpleMailMessage result = sentMessagesSimple.get(i);
        sentMessagesSimple.remove(i);
        return result;
    }

    /**
     * retrieve the last-sent "mime" mail message
     */
    public MimeMessage popMime() {
        int i = sentMessagesMime.size() - 1;
        MimeMessage result = sentMessagesMime.get(i);
        sentMessagesMime.remove(i);
        return result;
    }

    public int getSentMessageCount() {
        return sentMessagesSimple.size() + sentMessagesMime.size();
    }

    public void reset() {
        sentMessagesMime.clear();
        sentMessagesSimple.clear();
    }

}
