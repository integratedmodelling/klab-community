// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community;

import java.io.OutputStream;

import org.integratedmodelling.community.security.SecurityHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KLabLogger {

    private SecurityHelper securityHelper;

    private Logger logger;

    private static class MockSecurityHelper extends SecurityHelper {

        @Override
        public String getLoggedInUsername() {
            return null;
        }
    }

    public KLabLogger(Class<?> callingClass) {
        logger = LoggerFactory.getLogger(callingClass);
        securityHelper = new MockSecurityHelper(); // by default
    }

    public void setSecurityHelper(SecurityHelper securityHelper) {
        this.securityHelper = securityHelper;
    }

    public String debug(String msg) {
        return debug(securityHelper.getLoggedInUsername(), msg);
    }

    public String debug(String username, String msg) {
        if (username != null) {
            msg = String.format("[%s] " + msg, username);
        }
        logger.debug(msg);
        return msg;
    }

    public String info(String msg) {
        return info(securityHelper.getLoggedInUsername(), msg, null);
    }

    public String info(String msg, Throwable e) {
        return info(securityHelper.getLoggedInUsername(), msg, e);
    }

    public String info(String username, String msg, Throwable e) {
        if (username != null) {
            msg = String.format("[%s] " + msg, username);
        }
        logger.info(msg, e);
        return msg;
    }

    public String warn(String msg) {
        return warn(securityHelper.getLoggedInUsername(), msg, null);
    }

    public String warn(String msg, Throwable e) {
        return warn(securityHelper.getLoggedInUsername(), msg, e);
    }

    public String warn(String username, String msg, Throwable e) {
        if (username != null) {
            msg = String.format("[%s] " + msg, username);
        }
        logger.warn(msg, e);
        return msg;
    }

    public String error(String msg) {
        return error(securityHelper.getLoggedInUsername(), msg, null);
    }

    public String error(String msg, Throwable e) {
        return error(securityHelper.getLoggedInUsername(), msg, e);
    }

    public String error(String username, String msg, Throwable e) {
        if (username != null) {
            msg = String.format("[%s] " + msg, username);
        }
        logger.error(msg, e);
        return msg;
    }

    public void dumpOutputStream(OutputStream os) {
        String[] outputLines = os.toString().split("\n");
        for (String line : outputLines) {
            logger.info("| " + line);
        }
    }
}
