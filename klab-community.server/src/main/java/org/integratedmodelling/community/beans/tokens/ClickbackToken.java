// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans.tokens;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.integratedmodelling.community.beans.Role;
import org.integratedmodelling.community.configuration.CoreApplicationConfig;
import org.integratedmodelling.community.exceptions.TokenExpiredException;
import org.integratedmodelling.community.management.AuthManager;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Transient;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * TODO think! are non-clickback endpoints protected?
 *
 * A token used for embedding in a clickback URL, like what you would get in an email.
 * When redeeming (logging in with) a clickback token, the onLogin() method will be called
 * to perform whatever action should be taken (i.e. to perform the work of the clickback URL).
 *
 * @author luke
 */
public abstract class ClickbackToken extends AuthenticationToken {

    private static final long serialVersionUID = -8819538677554609017L;

    private static final int TOKEN_TTL_SECONDS = 60 * 60 * 48; // 48 hours

    private static final Collection<GrantedAuthority> ROLES = new ArrayList<>(
            Arrays.asList(Role.ROLE_CLICKBACK));

    @Transient
    private URL clickbackUrl;

    public ClickbackToken(String username) {
        super(username);
        expiration = DateTime.now().plusSeconds(TOKEN_TTL_SECONDS);
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return ROLES;
    }

    @Override
    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
    }

    /**
     * client-clickback URL by default (due to coreApplicationConfig.getClientClickbackUrl())
     *
     * override this for token types that can hit server clickback URLs directly (i.e. GET from email click)
     */
    public void setClickbackUrl(CoreApplicationConfig coreApplicationConfig) {
        setClickbackUrl(coreApplicationConfig.getClientClickbackUrl(getClickbackAction(), getTokenString()));
    }

    protected void setClickbackUrl(URL clickbackUrl) {
        this.clickbackUrl = clickbackUrl;
    }

    public URL getClickbackUrl() {
        return clickbackUrl;
    }

    public abstract ClickbackAction getClickbackAction();

    /**
     * Vaildate token and do whatever the clickback link is supposed to do.
     * The user will not be fully logged in (because ClickbackToken has a limited role),
     * but a UserDetails object can still be loaded from the security context.
     */
    public final void handleClickback(AuthManager authManager) {
        if (isExpired()) {
            throw new TokenExpiredException("Token is expired: " + getTokenString());
        }

        SecurityContextHolder.getContext().setAuthentication(this);
        handleClickbackAction(authManager);
    }

    /**
     * override this to provide the token behavior
     */
    protected void handleClickbackAction(AuthManager authManager) {
        // generally, Clickback Tokens should delete themselves so they cannot be re-used.
        authManager.deleteToken(getTokenString());
    }

    public String getSuccessUrl(CoreApplicationConfig applicationConfig) {
        return applicationConfig.getSiteUrl().toString();
    }
}
