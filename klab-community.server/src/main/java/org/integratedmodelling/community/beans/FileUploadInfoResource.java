// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Any and all metadata associated with a file upload
 * (i.e. the fields that can be set by the upload form).
 * The fields do not have to be complete, but the UUID must be set.
 */
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileUploadInfoResource {

    // unique identifiers
    public UUID uuid;

    public String name;

    // human descriptions
    public String descriptionBrief;

    public String descriptonLong;

    // namespace
    public String continentCode;

    public String countryCode;

    public String regionCode;

    public String theme;
}
