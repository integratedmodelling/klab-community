// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * The user data we want "live" during a session. Returned upon successful login
 * and only kept at the server side.
 * @author Ferd
 * @author Luke
 */
public class UserData implements UserDetails {

    public static final String GLOBAL_GROUP = "REGISTERED";

    private static final long serialVersionUID = -6213593655742083476L;

    @Id
    String id;

    @Indexed(unique = true)
    String username;

    String email;

    String affiliation;

    String comments;

    @Indexed
    String firstName;

    @Indexed
    String lastName;

    String initials;

    String address;

    String jobTitle;

    String phone;

    String serverUrl;

    DateTime registrationDate;

    DateTime lastLogin;

    DateTime lastEngineConnection;

    boolean sendUpdates = true;

    final Set<Role> roles = new HashSet<>(); // LDAP security roles

    final Set<String> groups = new HashSet<>(); // research groups, etc. in web tool

    final Set<String> applications = new HashSet<>();

    AccountStatus accountStatus = AccountStatus.pendingActivation;

    public enum AccountStatus {
        active,
        locked,
        deleted,
        expired,
        pendingActivation,
    };

    // @Transient prevents the password from being stored in Mongo.
    // (although it's probably not a huge deal because this will be hashed in production.)
    @Transient
    String passwordHash;

    // TODO reference this from a AbstractSecurityInterceptor for DataSource object security.
    // see "secure object" references at http://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle
    public boolean hasAnyGroups(Set<String> groups) {
        for (String group : groups) {
            if (groups.contains(group)) {
                return true;
            }
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<Role> getAuthorities() {
        return new HashSet<>(roles);
    }

    @Override
    public boolean isAccountNonExpired() {
        return !accountStatus.equals(AccountStatus.expired);
    }

    @Override
    public boolean isAccountNonLocked() {
        return !accountStatus.equals(AccountStatus.locked);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return accountStatus.equals(AccountStatus.active);
    }

    public boolean isAdmin() {
        return isRole(Role.ROLE_ADMINISTRATOR);
    }

    public boolean isRole(Role role) {
        return roles.contains(role);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void addComments(String additionalComments) {
        if (comments == null) {
            comments = additionalComments;
        } else {
            comments += "\n" + additionalComments;
        }
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public void setSendUpdates(boolean sendUpdates) {
        this.sendUpdates = sendUpdates;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getEmail() {
        return email;
    }

    public void addRoles(Role... roles) {
        this.roles.addAll(Arrays.asList(roles));
    }

    public void setRoles(Collection<Role> roles) {
        this.roles.clear();
        this.roles.addAll(roles);
    }

    public void addGroups(String... groups) {
        addGroups(Arrays.asList(groups));
    }

    public void addGroups(List<String> groups) {
        this.groups.addAll(groups);
    }

    public void setGroups(Collection<String> groups) {
        this.groups.clear();
        this.groups.addAll(groups);
    }

    public Set<String> getGroups() {
        return new HashSet<>(groups);
    }

    public Set<String> getApplications() {
        return applications;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String url) {
        serverUrl = url;
    }

    public boolean shouldSendUpdates() {
        return sendUpdates;
    }

    /**
     * Deprecated - use getPasswordHash() instead.
     * this is here for compatibility with UserDetails. The securePassword field
     * should NEVER be persisted anywhere beyond this thread's memory.
     */
    @Override
    @Deprecated
    public String getPassword() {
        return getPasswordHash();
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public String toString() {
        return username + roles.toString();
    }

    public String getFullName() {
        String first = getFirstName();
        String last = getLastName();
        if (first == null && last == null) {
            return null;
        }
        // return both, or just one if the other is not available
        return String.format("%s %s", first == null ? "" : first, last == null ? "" : last).trim();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFromProfileResource(ProfileResource resource) {
        // TODO use ObjectMapper
        accountStatus = resource.accountStatus;
        address = resource.address;
        affiliation = resource.affiliation;
        comments = resource.comments;
        email = resource.email;
        firstName = resource.firstName;
        setGroups(resource.groups);
        initials = resource.initials;
        jobTitle = resource.jobTitle;
        lastName = resource.lastName;
        //password = resource.password;
        phone = resource.phone;
        sendUpdates = resource.sendUpdates;
        serverUrl = resource.serverUrl;
        //username = resource.username;
    }

    public boolean userGroupsOverlapWith(HashSet<String> groups) {
        if (groups == null) {
            // force this to be checked by set intersection, rather than instantly failing (preserves logic)
            groups = new HashSet<>();
        }

        if (groups.contains(UserData.GLOBAL_GROUP)) {
            return true;
        }

        Set<String> setIntersection = getGroups(); // returns a copy
        setIntersection.retainAll(groups);
        if (setIntersection.size() > 0) {
            return true;
        }

        return false;
    }

    /**
     * return full name (if available) or some sensible constructed value
     * so that LDAP/Crowd doesn't choke on a null value
     *
     * order of preference: full name, first name, username
     */
    public String getDisplayName() {
        if (getFirstName() != null && getLastName() != null) {
            return getFullName();
        }
        // either firstName or lastName is null.
        if (getFirstName() != null) {
            return getFirstName();
        }
        return getUsername();
    }

    public void setRegistrationDate() {
        registrationDate = DateTime.now();
    }

    public void setLastLogin() {
        lastLogin = DateTime.now();
    }

    public void setLastEngineConnection() {
        lastEngineConnection = DateTime.now();
    }
}
