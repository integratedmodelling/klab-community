// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans.tokens;

/**
 * Identifies actions which can be given in CLIENT clickback URLs, for example:
 * http://integratedmodelling.org/collaboration/#/clickback/activate/df59b4c2-ab66-4ad0-905d-e2afe7542021
 * where /activate/ identifies the action that will be taken in the browser when the link is clicked.
 *
 * The main purpose here is to identify valid actions, so that the controller can reject invalid clickback requests.
 *
 * It also associates each action with the type of token which must be used, but it is not used to verify
 * that the correct token type is used - that is done explicitly at whatever endpoint accepts the clickback request.
 *
 * NOTE: the values here must agree with CLICKBACK_CONFIG in main.js
 */
public enum ClickbackAction {
    activate(
            TokenType.activate),
    setPassword(
            TokenType.setPassword),;

    private final TokenType tokenType;

    public TokenType getTokenType() {
        return tokenType;
    }

    public Class<? extends AuthenticationToken> getClazz() {
        return tokenType.getClazz();
    }

    ClickbackAction(TokenType tokenType) {
        this.tokenType = tokenType;
    }
}
