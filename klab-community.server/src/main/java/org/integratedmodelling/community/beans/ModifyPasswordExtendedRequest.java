// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans;

import javax.naming.ldap.ExtendedRequest;
import javax.naming.ldap.ExtendedResponse;

// thank you:
// http://tech.wrighting.org/2013/06/using-the-ldap-password-modify-extended-operation-with-spring-ldap/
// http://sourceforge.net/p/jamm/git/ci/master/tree/jamm/src/backend/java/jamm/ldap/ModifyPasswordRequest.java

public class ModifyPasswordExtendedRequest implements ExtendedRequest {

    private static final int X_690_BER_SEQUENCE_TAG = 0x30;

    private static final long serialVersionUID = -9123332810593728386L;

    private static final byte LDAP_MODIFY_PASSWORD_DN_TAG = (byte) 0x80;

    private static final byte LDAP_MODIFY_PASSWORD_PASSWORD_TAG = (byte) 0x82;

    private String dn;

    private String password;

    public ModifyPasswordExtendedRequest(String dn, String password) throws NullPointerException {
        if (dn == null) {
            throw new NullPointerException("dn cannot be null");
        }

        if (password == null) {
            throw new NullPointerException("password cannot be null");
        }

        this.dn = dn;
        this.password = password;
    }

    @Override
    public String getID() {
        return "1.3.6.1.4.1.4203.1.11.1";
    }

    /**
     * Get the x.690 BER encoded value for this operation (byte lengths in parentheses):
     * sequence tag (1) sequence length (1) dn tag (1) dn length (1) dn (variable)
     * password tag (1) password length (1) password (variable)
     *
     * See https://en.wikipedia.org/wiki/X.690#BER_encoding
     */
    @Override
    public byte[] getEncodedValue() {
        byte[] passwordBytes = password.getBytes();
        byte[] dnBytes = dn.getBytes();

        // 6 single-byte tags/lengths in the encoded value
        byte[] result = new byte[6 + dnBytes.length + passwordBytes.length];
        int i = 0;

        result[i++] = (byte) X_690_BER_SEQUENCE_TAG;
        result[i++] = (byte) (4 + dnBytes.length + passwordBytes.length);

        result[i++] = LDAP_MODIFY_PASSWORD_DN_TAG;
        result[i++] = (byte) dnBytes.length;

        System.arraycopy(dnBytes, 0, result, i, dnBytes.length);
        i += dnBytes.length;

        result[i++] = LDAP_MODIFY_PASSWORD_PASSWORD_TAG;
        result[i++] = (byte) passwordBytes.length;

        System.arraycopy(passwordBytes, 0, result, i, passwordBytes.length);
        i += passwordBytes.length;

        return result;
    }

    /**
     * OpenLDAP doesn't use a response for extended password modify
     */
    @Override
    public ExtendedResponse createExtendedResponse(String id, byte[] berValue, int offset, int length) {
        return null;
    }
}
