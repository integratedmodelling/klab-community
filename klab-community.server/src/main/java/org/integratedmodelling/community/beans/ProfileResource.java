// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.Email;
import org.integratedmodelling.community.beans.UserData.AccountStatus;
import org.integratedmodelling.community.resource.validation.Constraints;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * A convenience class which identifies the "publicly accessible" fields from {@link UserData}.
 * The fields' names and types should match their counterparts in UserData,
 * with the exception of password vs. passwordHash.
 *
 * Instances of this class can be created from UserData by calling:
 * ObjectMapper.convertValue(userData, ProfileResource.class)
 *
 * TODO is there any risk in convertValue(profileResource, UserData.class)?
 */
@JsonAutoDetect
public class ProfileResource {

    @Size(max = Constraints.USERNAME_LENGTH)
    @Pattern(regexp = Constraints.USERNAME_PATTERN)
    public String username;

    @Email
    public String email;

    public String serverUrl;

    public String firstName;

    public String lastName;

    public String initials;

    public String address;

    public String jobTitle;

    public String phone;

    public String affiliation;

    public List<Role> roles = new ArrayList<>(); // LDAP security roles

    public List<String> groups = new ArrayList<>(); // research groups, etc. in web tool

    public boolean sendUpdates;

    public String comments;

    public DateTime registrationDate;

    public DateTime lastLogin;

    public DateTime lastEngineConnection;

    public AccountStatus accountStatus;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(username).append(email).append(serverUrl).append(firstName).append(lastName)
                .append(initials).append(address).append(jobTitle).append(phone).append(affiliation).append(groups)
                .append(sendUpdates).append(comments).append(accountStatus).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        ProfileResource other = (ProfileResource) obj;
        return new EqualsBuilder().append(username, other.username).append(email, other.email)
                .append(serverUrl, other.serverUrl).append(firstName, other.firstName).append(lastName, other.lastName)
                .append(initials, other.initials).append(address, other.address).append(jobTitle, other.jobTitle)
                .append(phone, other.phone).append(affiliation, other.affiliation).append(groups, other.groups)
                .append(sendUpdates, other.sendUpdates).append(comments, other.comments)
                .append(accountStatus, other.accountStatus).isEquals();
    }
}
