// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans.tokens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.integratedmodelling.community.beans.Role;
import org.joda.time.DateTime;
import org.springframework.security.core.GrantedAuthority;

public class EngineToken extends AuthenticationToken {

    private static final long serialVersionUID = -4433591204500903916L;

    private static final int TOKEN_TTL_SECONDS = 60 * 60 * 24; // 24 hours (reboot k.LAB or have a retry hook to refresh)

    private static final Collection<GrantedAuthority> ROLES = new ArrayList<>(
            Arrays.asList(Role.ROLE_ENGINE));

    public EngineToken(String username) {
        super(username);
        expiration = DateTime.now().plusSeconds(TOKEN_TTL_SECONDS);
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return ROLES;
    }

    @Override
    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
    }
}
