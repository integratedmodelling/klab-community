// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.DateTime;

public class ModificationEvent implements Comparable<ModificationEvent> {

    public enum Type {
        created,
        deletedFile,
        error,
        finalizedUpload,
        importNotes,
        movedFile,
        prunedBrokenLink,
        uploaded,
        updatedMetadata,
    }

    public DateTime modificationTime;

    public String username;

    public Type eventType;

    public String notes;

    public ModificationEvent() {
        // for Jackson
    }

    public ModificationEvent(DateTime modificationTime, String username, Type eventType) {
        this.modificationTime = modificationTime;
        this.username = username;
        this.eventType = eventType;
    }

    @Override
    public int compareTo(ModificationEvent other) {
        return modificationTime.compareTo(other.modificationTime);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof ModificationEvent)) {
            return false;
        }
        return this.hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(eventType).append(modificationTime).append(notes).append(username)
                .hashCode();
    }

    @Override
    public String toString() {
        DateTime date = new DateTime(modificationTime);
        String msg = String.format("(%d/%d: %s %s. %s)", date.getMonthOfYear(), date.getDayOfMonth(), username,
                eventType, notes);
        return msg;
    }
}
