// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.beans;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import org.integratedmodelling.community.resource.validation.Constraints;
import org.integratedmodelling.community.resource.validation.Publishable;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BasicDataSource implements Serializable {

    private static final long serialVersionUID = 1791022088667343741L;

    private static final String DEFAULT_PRIMARY_SEPARATOR = ":";

    private static final String DEFAULT_NAMESPACE_SEPARATOR = ".";

    private static final String GEOSERVER_NAMESPACE_SEPARATOR = "-";

    /**
     * MongoDB unique ID
     */
    @Id
    public String id;

    /**
     * java.util.UUID which was generated for the upload form
     */
    @Indexed(unique = true)
    public String uuid;

    /**
     * User-friendly name (one word, usable as identifier, and also unique).
     */
    @Pattern(regexp = Constraints.DATASOURCE_NAME_PATTERN)
    @NotEmpty(message = "Data Sources must have a name", groups = Publishable.class)
    public String name;

    /**
     * topic of the data (e.g. hydrology). always non-null.
     */
    @NotEmpty(message = "Data Sources must have a theme", groups = Publishable.class)
    public String theme;

    /**
     * subtype of the data source; constrained by the general type (e.g. Geo > vector)
     */
    public DataType type;

    /**
     * which group is allowed to view/download/use this data set NOTE: currently, only
     * using a single-group concept. (data sources are shared with at most one group)
     */
    public HashSet<String> permittedGroups;

    /**
     * whether or not *all* users can view/download/use this data set. If true, then
     * permittedGroups is not used. (but it remains set so that the last-set value can be
     * reverted to easily)
     */
    public boolean isPublic = false;

    /**
     * Server on which we are assigned. Based on the type, we should have a default server
     * even if nobody has explicitly assigned one. Null in this field means we don't know
     * how to publish a source of this type. Having a server assigned doesn't mean we are
     * published.
     */
    public String serverId;

    public DateTime createdDate;

    /**
     * username of the person who created this data source
     */
    public String creator;

    /**
     * Enabled or disabled by user. What happens at enable/disable depends on the
     * DataManager - typically it will be published/unpublished to a server.
     */
    public boolean enabled = false;

    /**
     * if true, the linked source is newer than the date of last modification of the
     * source descriptor. Can only be assessed at server side, but is communicated to the
     * client through this field.
     */
    public boolean needsRefresh;

    /**
     * Brief description - suitable for one-line display. May use Markdown.
     */
    public String descriptionBrief;

    /**
     * true if published in a DB
     */
    public boolean isDB;

    /**
     * Initially, a data set is pending/temporary (with isFinalized == false), meaning its
     * files are stored in temporary storage and its metadata is not visible on the
     * standard views
     */
    public boolean isFinalized = false;

    /**
     * should be set to 'true' after reading from the database if the user has at least
     * one group this data source is shared with (does not include administrators without
     * group overlap)
     */
    @Transient
    public boolean userHasOnePermittedGroup = false;

    /**
     * should be set to 'true' after reading from the database if the user has permission
     * to view (including admin rights)
     */
    @Transient
    public boolean readable = false;

    /**
     * should be set to 'true' after reading from the database if the user has permission
     * to edit/delete/etc (including admin rights)
     */
    @Transient
    public boolean writeable = false;

    @Transient
    public URL url;

    public String continentCode;

    public String countryCode;

    public String regionCode;

    public String projection;

    public boolean needsImport;

    @JsonProperty
    public String getUrn() {
        return getUrn(DEFAULT_PRIMARY_SEPARATOR, DEFAULT_NAMESPACE_SEPARATOR);
    }

    @JsonProperty
    public String getNamespace() {
        return getNamespace(DEFAULT_NAMESPACE_SEPARATOR);
    }

    @JsonProperty
    public String getGeoserverNamespace() {
        // HACK: usernames have dots in them; this replacement should be based on
        // Constraints.java somehow.
        return getUserSpecificNamespace(GEOSERVER_NAMESPACE_SEPARATOR).replace(".", GEOSERVER_NAMESPACE_SEPARATOR);
    }

    public String getUrn(String primarySeparator, String namespaceSeparator) {
        String namespace = getNamespace(namespaceSeparator);
        if (StringUtils.isEmpty(serverId) || StringUtils.isEmpty(creator) || StringUtils.isEmpty(name)
                || StringUtils.isEmpty(namespace)) {
            return null;
        }
        return String.format("%s%s%s%s%s%s%s", serverId, primarySeparator, creator, primarySeparator, namespace,
                primarySeparator, name);
    }

    public String getUserSpecificNamespace(String separator) {
        return String.format("%s%s%s", creator, separator, getNamespace(separator));
    }

    /**
     * @see DataSourceManager#getByUrn(String)
     */
    public String getNamespace(String separator) {
        List<String> namespace = new ArrayList<>();
//        if (StringUtils.isEmpty(continentCode)) {
//            namespace.add(Continent.GLOBAL);
//        } else {
//            namespace.add(continentCode.toLowerCase());
//            if (!StringUtils.isEmpty(countryCode)) {
//                namespace.add(countryCode.toLowerCase());
//                if (!StringUtils.isEmpty(regionCode)) {
//                    namespace.add(regionCode.toLowerCase());
//                }
//            }
//        }
        namespace.add(theme == null ? "" : theme.toLowerCase());
        return StringUtils.join(namespace, separator);
    }

    @Override
    public String toString() {
        return "DataSource:" + name == null ? "<unnamed>" : name;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(createdDate).append(creator).append(descriptionBrief).append(enabled)
                .append(id).append(isDB).append(isFinalized).append(isPublic).append(name).append(needsRefresh)
                .append(permittedGroups).append(serverId).append(theme).append(type).append(uuid).append(continentCode)
                .append(countryCode).append(needsImport).append(projection).append(regionCode).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        BasicDataSource other = (BasicDataSource) obj;
        return new EqualsBuilder().append(createdDate, other.createdDate).append(creator, other.creator)
                .append(descriptionBrief, other.descriptionBrief).append(enabled, other.enabled).append(id, other.id)
                .append(isDB, other.isDB).append(isFinalized, other.isFinalized).append(isPublic, other.isPublic)
                .append(name, other.name).append(needsRefresh, other.needsRefresh)
                .append(permittedGroups, other.permittedGroups).append(serverId, other.serverId)
                .append(theme, other.theme).append(type, other.type).append(uuid, other.uuid)
                .append(continentCode, other.continentCode).append(countryCode, other.countryCode)
                .append(needsImport, other.needsImport).append(projection, other.projection)
                .append(regionCode, other.regionCode).isEquals();
    }
}
