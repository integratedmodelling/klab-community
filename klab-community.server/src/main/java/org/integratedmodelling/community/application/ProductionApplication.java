package org.integratedmodelling.community.application;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableAutoConfiguration
@EnableMongoRepositories({"org.integratedmodelling.community.persistence"})
@ComponentScan(
        basePackages = {
                "org.integratedmodelling.community.configuration.production",
                "org.integratedmodelling.community.persistence",
                "org.integratedmodelling.community.security",
                "org.integratedmodelling.community.management",
                "org.integratedmodelling.community.resource.annotation",
                "org.integratedmodelling.community.resource.notification",
                "org.integratedmodelling.community.resource.validation",
                "org.integratedmodelling.community.resource.publish"
})
public class ProductionApplication {
    
    public static void main(String[] args) throws IOException {
        // final ConfigurableApplicationContext context =
        SpringApplication.run(ProductionApplication.class, args);
    }

}
