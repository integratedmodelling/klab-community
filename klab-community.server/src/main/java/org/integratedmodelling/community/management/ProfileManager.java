// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.management;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.community.beans.ProfileResource;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.beans.tokens.VerifyEmailClickbackToken;
import org.integratedmodelling.community.exceptions.UnauthorizedException;
import org.integratedmodelling.community.persistence.UserRepository;
import org.integratedmodelling.community.security.KLabUserDetailsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProfileManager extends Manager {

    @Autowired
    private AuthManager authManager;

    @Autowired
    private EmailManager emailManager;

    @Autowired
    private KLabUserDetailsManager userDetailsManager;

    @Autowired
    private UserRepository userRepository;

    /**
     * the canonical profile update (manager) method.
     */
    public ProfileResource updateProfile(ProfileResource profileResource) {
        UserData loggedInUser = securityHelper.getLoggedInUserData();
        dataUtil.cleanse(profileResource);

        UserData userData;
        logger.info("Attempting to update profile for user: " + profileResource.username);
        if (loggedInUser.getUsername().equals(profileResource.username)) {
            if (!loggedInUser.isAdmin()) {
                // a normal user is modifying him/herself. Enforce security rules.
                profileResource.accountStatus = loggedInUser.getAccountStatus();
                profileResource.groups = new ArrayList<>(loggedInUser.getGroups());
                profileResource.roles = new ArrayList<>(loggedInUser.getAuthorities());
            }

            userData = loggedInUser;

            // email changes must be done via email-verify clickback
            String existingEmail = userData.getEmail();
            if (!profileResource.email.equals(existingEmail)) {
                VerifyEmailClickbackToken token = authManager.createVerifyEmailClickbackToken(profileResource.username,
                        profileResource.email);
                emailManager.sendVerifyEmailClickback(existingEmail, token.getClickbackUrl());

                // don't actually change it yet
                profileResource.email = existingEmail;
            }
        } else {
            // the user is modifying someone else. Make sure that's ok.
            // this should be logged by Spring
            if (!loggedInUser.isAdmin()) {
                throw new UnauthorizedException(
                        String.format("The currently logged in user does not have access to modify user '%s'.",
                                profileResource.username));
            }

            // throws if not found, meaning that an existing record is required.
            // (because 'create' is a very special thing)
            userData = securityHelper.getUserData(profileResource.username);
        }

        // NOTE: at this point, we know that either:
        // - user is modifying him/herself and groups are NOT changing
        // - user is an admin modifying someone else and groups MAY BE changing.

        userData.setFromProfileResource(profileResource);
        userDetailsManager.updateUser(userData);

        // re-apply anything that may have resulted from setFromProfileResource() (like filtering/merging/etc)
        profileResource = objectMapper.convertValue(userData, ProfileResource.class);
        return profileResource;
    }

    public Map<String, ProfileResource> getAllProfiles() {
        List<UserData> users = userRepository.findAll();

        Map<String, ProfileResource> result = new HashMap<>();
        for (UserData userData : users) {
            decorate(userData);
            result.put(userData.getUsername(), objectMapper.convertValue(userData, ProfileResource.class));
        }

        return result;
    }

    public void decorate(UserData userData) {
        if (userData.getServerUrl() == null) {
            userData.setServerUrl(coreApplicationConfig.getEngineUrl().toExternalForm());
        }
    }
}
