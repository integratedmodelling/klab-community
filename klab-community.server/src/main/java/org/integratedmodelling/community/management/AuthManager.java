// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.management;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;

import org.apache.commons.lang.RandomStringUtils;
import org.bouncycastle.openpgp.PGPException;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.auth.User;
import org.integratedmodelling.common.beans.auth.UserProfile;
import org.integratedmodelling.common.beans.auth.UserProfile.AccountStatus;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.community.beans.Role;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.beans.tokens.ActivateAccountClickbackToken;
import org.integratedmodelling.community.beans.tokens.AuthenticationToken;
import org.integratedmodelling.community.beans.tokens.ChangePasswordClickbackToken;
import org.integratedmodelling.community.beans.tokens.ClickbackToken;
import org.integratedmodelling.community.beans.tokens.EngineToken;
import org.integratedmodelling.community.beans.tokens.VerifyEmailClickbackToken;
import org.integratedmodelling.community.configuration.WebSecurityConfig;
import org.integratedmodelling.community.exceptions.AuthenticationFailedException;
import org.integratedmodelling.community.exceptions.BadRequestException;
import org.integratedmodelling.community.exceptions.TokenGenerationException;
import org.integratedmodelling.community.exceptions.UserEmailExistsException;
import org.integratedmodelling.community.exceptions.UserExistsException;
import org.integratedmodelling.community.exceptions.UsernameNotFoundException;
import org.integratedmodelling.community.persistence.TokenRepository;
import org.integratedmodelling.community.persistence.UserRepository;
import org.integratedmodelling.community.security.KLabUserDetailsManager;
import org.integratedmodelling.community.security.SecurityHelper;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Primary
public class AuthManager extends Manager /* implements UserAuthorizationProvider */ {

    // TODO delete this when users all have the new certs
    private static final String LEGACY_CERT_FILE_EXPIRATION = "2015-06-01";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private EmailManager emailManager;

    @Autowired
    private LicenseManager licenseManager;

    @Autowired
    private SecurityHelper securityHelper;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KLabUserDetailsManager userDetailsManager;

    @Autowired
    private WebSecurityConfig webSecurityConfig;

//    @Autowired
//    IUserRegistry userRegistry;

    public void activateAccount() {
        // the user is already authenticated via clickback token header, but we want
        // to do some extra verification because they aren't supplying a password
        Authentication authentication = securityHelper.getLoggedInAuthentication();
        if (!(authentication instanceof ActivateAccountClickbackToken)) {
            throw new AuthenticationFailedException("The token submitted was not valid for activating an account.");
        }

        // this will also verify that the account started in pendingActivation
        userDetailsManager.activateUser(securityHelper.getLoggedInUsername());
    }

    @SuppressWarnings("serial")
    public AuthenticationToken authenticate(String username, String password) {
        AuthenticationToken result = null;
        username = dataUtil.cleanseUsername(username);
        try {
            // NOTE: this is not created with createAuthenticationToken() because it's not
            // saved to the DB
            Authentication authRequest = new UsernamePasswordAuthenticationToken(username, password);

            // BEGIN TODO put this into AUTH library
            Authentication authResult = authenticationManager.authenticate(authRequest);
            logger.info(username, "Authenticated user via password authentication " + authResult.getAuthorities(),
                    null);

            if (!authResult.isAuthenticated()) {
                String msg = "Something went wrong with authentication. Result.isAuthenticated() == false, but no exception was thrown.";
                logger.error(msg + " Throwing now...");
                throw new AuthenticationException(msg) {

                    /**
                     *
                     */
                    private static final long serialVersionUID = -8517575154826926750L;
                };
            } else {
                deleteExpiredTokens(username);

                // generate a new token (even if there's already a current one) and save
                // it to MongoDB
                // this is because a token represents "one successful request to the
                // authenticate endpoint"
                result = createAuthenticationToken(username, AuthenticationToken.class);

                // register the authentication with Spring Security
                SecurityContextHolder.getContext().setAuthentication(result);

                updateLastLogin(username);
            }
            // END TODO
        } catch (AuthenticationException e) {
            String msg = "Login failed for user: " + username;
            logger.error(username, msg, e);
            throw new AuthenticationFailedException(msg, e);
        }
        return result;
    }

    private void updateLastLogin(String username) {
        UserData userData = userDetailsManager.loadUserByUsername(username);
        userData.setLastLogin();
        userDetailsManager.updateUser(userData);
    }

    private void updateLastEngineConnection(String username) {
        UserData userData = userDetailsManager.loadUserByUsername(username);
        userData.setLastEngineConnection();
        userDetailsManager.updateUser(userData);
    }

    public AuthenticationToken authenticateWithCertFile(String certFileContent) throws IOException, PGPException {
        Properties properties = licenseManager.readCertFileContent(certFileContent);
        String username = properties.getProperty(LicenseManager.Fields.USER);
        logger.info(String.format("Attempting to authenticate user %s via cert file...", username));

        String expiryString = properties.getProperty(LicenseManager.Fields.EXPIRY);

        // TODO delete this when users all have the new certs
        if (expiryString == null) {
            expiryString = LEGACY_CERT_FILE_EXPIRATION;
        }

        DateTime expiry = DateTime.parse(expiryString);
        if (expiry.isBeforeNow()) {
            String msg = String.format("The cert file submitted for user %s is expired.", username);
            logger.error(msg);
            throw new AuthenticationFailedException(msg);
        }

        deleteExpiredTokens(username);

        logger.info(
                String.format("Successfully authenticated user %s via cert file. Generating EngineToken...", username));

        AuthenticationToken result = createAuthenticationToken(username, EngineToken.class);
        updateLastEngineConnection(username);
        return result;
    }

    /**
     * ALL tokens should be created with these two methods TODO move this to auth library
     */
    public AuthenticationToken createAuthenticationToken(String username,
            Class<? extends AuthenticationToken> tokenType) {
        if (ClickbackToken.class.isAssignableFrom(tokenType)) {
            throw new TokenGenerationException(
                    "ClickbackTokens must be generated by createClickbackToken(), not createAuthenticationToken()",
                    null);
        }

        AuthenticationToken result = null;
        username = dataUtil.cleanseUsername(username);
        try {
            result = tokenType.getConstructor(String.class).newInstance(username);
        } catch (Exception e) {
            // mainly this is to throw RuntimeException instead of a checked exception
            // (shouldn't get here though)
            throw new TokenGenerationException("Unable to get token constructor method.", e);
        }

        UserData userData = userDetailsManager.loadUserByUsername(username);
        result.setAuthorities(userData.getAuthorities());
        result.setAuthenticated(true);

        tokenRepository.save(result);

        return result;
    }

    /**
     * ALL tokens should be created with these two methods TODO move this to auth library
     */
    public ClickbackToken createClickbackToken(String username, Class<? extends ClickbackToken> tokenType) {
        ClickbackToken result = null;
        username = dataUtil.cleanseUsername(username);
        try {
            result = tokenType.getConstructor(String.class).newInstance(username);
        } catch (Exception e) {
            // mainly this is to throw RuntimeException instead of a checked exception
            // (shouldn't get here though)
            throw new TokenGenerationException("Unable to get token constructor method.", e);
        }

        result.setClickbackUrl(coreApplicationConfig);
        result.setAuthenticated(true);

        tokenRepository.save(result);

        return result;
    }

    /**
     * email-verification-specific wrapper for createClickbackToken()
     */
    public VerifyEmailClickbackToken createVerifyEmailClickbackToken(String username, String email) {
        VerifyEmailClickbackToken token = (VerifyEmailClickbackToken) createClickbackToken(username,
                VerifyEmailClickbackToken.class);
        token.setNewEmailAddress(email);
        tokenRepository.save(token);
        return token;
    }

    /**
     * The user is logged in via username/password
     */
    public void changePassword(String oldPassword, String newPassword) {
        String username = securityHelper.getLoggedInUsername();
        UsernamePasswordAuthenticationToken usernamePassword = new UsernamePasswordAuthenticationToken(username,
                oldPassword);
        Authentication authResult = authenticationManager.authenticate(usernamePassword);
        if (!authResult.isAuthenticated()) {
            String msg = String.format("Old password was incorrect while trying password change for user '%s'.",
                    username);
            logger.error(msg);
            throw new AuthenticationFailedException(msg);
        }

        setPasswordAndSendVerificationEmail(newPassword);
    }

    public void createNewUser(String username, String email) throws TokenGenerationException {
        UserData userData = null;
        try {
            userData = userDetailsManager.loadUserByUsername(username);
        } catch (UsernameNotFoundException e) {
        }

        if (userData != null) {
            if (!AccountStatus.pendingActivation.equals(userData.getAccountStatus())
                    || !email.equals(userData.getEmail())) {
                // an existing account is in Mongo with either a mis-matched email or is
                // not in
                // 'pendingActivation' state
                // so don't let the user re-register. (Active users without LDAP records
                // can 'reset password'
                // instead)
                throw new BadRequestException("An account with this username already exists.\n"
                        + "If you are re-sending a registration email, please use the same email address as before.");
            }

            // the existing user account is pendingActivation and the email matches,
            // so it's OK to register as if it's a new account
        } else {
            // user is brand new
            userData = new UserData();
            userData.setUsername(username);
            userData.setEmail(email);
            userData.setRoles(Arrays.asList(Role.ROLE_USER));
            userData.setRegistrationDate();

            try {
                userDetailsManager.createMongoUser(userData);
            } catch (UserExistsException | UserEmailExistsException e) {
                throw new BadRequestException(e.getMessage(), e);
            }
        }

        ClickbackToken clickbackToken = createClickbackToken(username, ActivateAccountClickbackToken.class);
        emailManager.sendNewUser(email, username, clickbackToken.getClickbackUrl());
    }

    public UserData addUserToGroups(String username, List<String> groups) {
        UserData userData = securityHelper.getUserData(username);
        userData.addGroups(groups);
        userDetailsManager.updateUser(userData);
        return userData;
    }

    public void deleteToken(String tokenString) {
        AuthenticationToken dbToken = tokenRepository.findByTokenString(tokenString);
        try {
            tokenRepository.delete(dbToken);
        } catch (IllegalArgumentException e) {
            // client's localStorage was probably in a funky state.
            // nothing really to do here.
        }
    }

    private void deleteExpiredTokens(String username) {
        username = dataUtil.cleanseUsername(username);
        List<AuthenticationToken> dbTokens = tokenRepository.findByUsername(username);
        for (AuthenticationToken dbToken : dbTokens) {
            if (dbToken.isExpired()) {
                tokenRepository.delete(dbToken);
            }
        }
    }

    public ClickbackToken handleClickbackToken(String tokenString) {
        ClickbackToken token = (ClickbackToken) tokenRepository.findByTokenString(tokenString);
        if (token == null) {
            throw new BadRequestException("The token submitted could not be found.");
        }
        token.handleClickback(this);
        return token;
    }

    public void sendPasswordResetEmail(String username, String email, boolean invalidateCurrentPassword)
            throws MessagingException {
        UserData user = userDetailsManager.loadUserByUsername(username);
        if (email == null || !email.equals(user.getEmail())) {
            throw new AuthenticationFailedException("Could not find a user with that username and email address.");
        }

        if (invalidateCurrentPassword) {
            userDetailsManager.changePasswordForUser(username, RandomStringUtils.randomAlphanumeric(15));
        }

        ClickbackToken token = createClickbackToken(username, ChangePasswordClickbackToken.class);
        emailManager.sendPasswordReset(email, token.getClickbackUrl());
    }

    private void setPasswordAndSendVerificationEmail(String newPassword) {
        // The user will have logged in via PasswordChangeClickbackToken or
        // username/password.
        UserData persistedUser = securityHelper.getLoggedInUserData();
        if (persistedUser == null) {
            throw new BadRequestException("Could not find a user with the token that was submitted.");
        } else if (!AccountStatus.active.equals(persistedUser.getAccountStatus())) {
            throw new BadRequestException("An active user could not be found with the token that was submitted.");
        } else {
            // in case the user is changing their password to solve a broken state (i.e.
            // Mongo but no LDAP)
            // this will prevent the missing LDAP record from breaking the process
            if (!userDetailsManager.springUserExists(persistedUser.getUsername())) {
                userDetailsManager.createSpringUser(persistedUser);
            }

            userDetailsManager.changePassword(null, newPassword);

            // send an email notifying the user their password was changed
            emailManager.sendPasswordChangeConfirmation(persistedUser.getEmail());
        }
    }

    /**
     * The user is logged in by ActivateAccountClickbackToken or
     * ChangePasswordClickbackToken, and their account should be 'active' by now.
     */
    public void setPasswordWithTokenVerification(String newPassword) {
        // the user is already authenticated via clickback token header, but we want
        // to do some extra verification because they aren't supplying an old password
        Authentication authentication = securityHelper.getLoggedInAuthentication();
        if (!(authentication instanceof ChangePasswordClickbackToken)) {
            throw new AuthenticationFailedException("The token submitted was not valid for setting a new password.");
        }
        setPasswordAndSendVerificationEmail(newPassword);

        // manually delete the token because the normal clickback mechanism won't be doing
        // it
        deleteToken(((ChangePasswordClickbackToken) authentication).getTokenString());
    }

    public void setEmailWithTokenVerification(String newEmail) {
        // The user will have logged in via PasswordChangeClickbackToken or
        // username/password.
        UserData persistedUser = securityHelper.getLoggedInUserData();
        if (persistedUser == null) {
            throw new BadRequestException("Could not find a user with the token that was submitted.");
        } else if (!AccountStatus.active.equals(persistedUser.getAccountStatus())) {
            throw new BadRequestException("An active user could not be found with the token that was submitted.");
        } else {
            String oldEmail = persistedUser.getEmail();
            persistedUser.setEmail(newEmail);
            userDetailsManager.updateUser(persistedUser);
            emailManager.sendEmailChangeConfirmation(oldEmail, persistedUser.getUsername());
        }
    }

//    @Override
    public IUser getAuthenticatedUser(String token) throws KlabAuthorizationException {
        AuthenticationToken storedToken = tokenRepository.findByTokenString(token);
        if (storedToken != null && storedToken.isAuthenticated()) {
            UserData userData = securityHelper.getUserData(storedToken.getPrincipal());
            return new User(getAuthorizationResponse(userData, storedToken));
        }
        return null;
    }

    private AuthorizationResponse getAuthorizationResponse(UserData userData, AuthenticationToken token) {
        AuthorizationResponse ret = new AuthorizationResponse();
        UserProfile profile = new UserProfile();

        profile.setUsername(userData.getUsername());
        profile.setAccountStatus(org.integratedmodelling.common.beans.auth.UserProfile.AccountStatus
                .valueOf(userData.getAccountStatus().name()));
        profile.getGroups().addAll(userData.getGroups());
        profile.setEmail(userData.getEmail());
        // TODO finish

        ret.setProfile(profile);
        ret.setAuthToken(token.getTokenString());

        return ret;
    }

//    @Override
    public AuthorizationResponse authenticateUser(String username, String password) throws KlabAuthorizationException {
        AuthenticationToken token = authenticate(username, password);
        UserData userData = securityHelper.getUserData(token.getPrincipal());
        return getAuthorizationResponse(userData, token);
    }

//    @Override
    public AuthorizationResponse authenticateUser(String certificate) throws KlabAuthorizationException {
        AuthenticationToken token;
        try {
            token = authenticateWithCertFile(certificate);
        } catch (IOException | PGPException e) {
            throw new KlabAuthorizationException(e);
        }
        UserData userData = securityHelper.getUserData(token.getPrincipal());
        return getAuthorizationResponse(userData, token);
    }

//    @Override
//    public ConnectionAuthorization getAuthorization(String token) {
//        
//        /*
//         * this is the important piece
//         * FIXME either remove the recursive injection in knode NC or remove the
//         * dep on network controller.
//         */
//        ConnectionAuthorization ret = userRegistry.getConnectionAuthorization(token);
//        if (ret != null) {
//            return ret;
//        }
//        try {
//
//            IUser user = getAuthenticatedUser(token);
//            if (user == null) {
//                return null;
//            }
//            return new ConnectionAuthorization(user);
//
//        } catch (KlabAuthorizationException e) {
//            throw new KlabRuntimeException(e);
//        }
//    }

//    @Override
//    public boolean isAuthorizationDirect() {
//        return true;
//    }

}
