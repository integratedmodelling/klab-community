// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.management;

import java.io.File;
import java.net.URL;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.integratedmodelling.community.configuration.CoreApplicationConfig;
import org.integratedmodelling.community.configuration.EmailConfig;
import org.integratedmodelling.community.configuration.WebSecurityConfig;
import org.integratedmodelling.community.exceptions.TokenGenerationException;
import org.integratedmodelling.community.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailManager extends Manager {

    // TODO is there any benefit in combining this with JS constants?
    private static final String LOGIN_ROUTE = "/login";

    private static final int MAX_DAYS_FOR_RECENT_ANNOUNCEMENT_RESULTS = 60;

    @Autowired
    private EmailConfig emailConfig;

    @Autowired
    private WebSecurityConfig securityConfig;

    @Autowired
    private CoreApplicationConfig coreApplicationConfig;

    @Autowired
    private UserRepository userRepository;

//    @Autowired
//    private SystemAnnouncementRepository systemAnnouncementRepository;

    @Autowired
    private JavaMailSender mailSender;

    public void sendVerifyEmailClickback(String to, URL clickbackUrl) {
        String subject = String.format("Email verify for %s", coreApplicationConfig.getSiteName());
        String msg = String.format(
                "To verify this email address for %s, click on the following link: %s\n\n"
                        + "If the link does not work, try copying and pasting it into your browser.\n\n"
                        + "If you did not request that this email be used for this site, "
                        + "please let us know by replying to this email.",
                coreApplicationConfig.getSiteName(), clickbackUrl);
        logger.info("Sending email verification email to " + to + "...");
        send(emailConfig.replyableAdminEmailAddress(), to, subject, msg);
    }

    public void sendEmailChangeConfirmation(String to, String username) {
        String subject = String.format("The email address for '%s' at %s has been changed", username,
                coreApplicationConfig.getSiteName());
        String msg = String.format(
                "The email address for the account '%s' at %s has been changed.\n\n"
                        + "If you did not initiate this change, then you should update your email address as soon as possible.\n\n"
                        + "Log in at: %s/#%s",
                username, coreApplicationConfig.getSiteName(), coreApplicationConfig.getSiteUrl(), LOGIN_ROUTE);
        logger.info("Sending emailAddress change confirmation email to " + to + "...");
        send(emailConfig.replyableGeneralEmailAddress(), to, subject, msg);
    }

    public void sendPasswordChangeConfirmation(String to) {
        String subject = String.format("Your password for %s has been changed", coreApplicationConfig.getSiteName());
        String msg = String.format(
                "Your password for %s has been changed.\n\n"
                        + "If you did not initiate this password change, then you should reset your password as soon as possible.\n\n"
                        + "Log in at: %s/#%s",
                coreApplicationConfig.getSiteName(), coreApplicationConfig.getSiteUrl(), LOGIN_ROUTE);
        logger.info("Sending password change confirmation email to " + to + "...");
        send(emailConfig.replyableGeneralEmailAddress(), to, subject, msg);
    }

    public void sendPasswordReset(String to, URL clickbackUrl) throws MessagingException {
        String subject = String.format("Password reset for %s", coreApplicationConfig.getSiteName());
        String msg = String.format(
                "To reset your password for %s, click on the following link: %s\n\n"
                        + "If the link does not work, try copying and pasting it into your browser.\n\n"
                        + "If you did not request a password reset, please let us know by replying to this email.",
                coreApplicationConfig.getSiteName(), clickbackUrl);
        logger.info(String.format("Sending password reset email to %s with clickbackUrl %s ...", to, clickbackUrl));
        send(emailConfig.replyableAdminEmailAddress(), to, subject, msg);
    }

    public void sendNewUser(String to, String username, URL clickbackUrl) throws TokenGenerationException {
        String subject = String.format("Welcome to %s!", coreApplicationConfig.getSiteName());
        String msg = String.format(
                "You have successfully registered at %s. Your username is %s."
                        + "\n\nTo activate your account, click on the following link: %s"
                        + "\n\nIf you did not create an account here, please let us know by replying to this email.",
                coreApplicationConfig.getSiteName(), username, clickbackUrl.toExternalForm());
        logger.info("Sending new user email to " + to + "...");
        send(emailConfig.replyableAdminEmailAddress(), to, subject, msg);
    }

    public void sendFromMainEmailAddress(String to, String subject, String msg, File... attachments)
            throws MessagingException {
        send(emailConfig.replyableGeneralEmailAddress(), to, subject, msg, attachments);
    }

//    /**
//     * TODO should this prefix with "SYSTEM ANNOUNCEMENT: "? (currently: no)
//     *
//     * @return the announcement sent
//     */
//    public SystemAnnouncement sendSystemAnnouncement(SystemAnnouncement announcement) {
//        if (announcement.groups == null) {
//            announcement.groups = new HashSet<>();
//        }
//
//        if (announcement.groups.isEmpty() && !announcement.isPublic) {
//            throw new BadRequestException(
//                    String.format("Unable to send SystemAnnouncement '%s' because there are no recipients specified.",
//                            announcement.subject));
//        }
//
//        List<UserData> allUsers = userRepository.findAll();
//        announcement.id = null;
//        announcement.started = DateTime.now();
//        announcement = systemAnnouncementRepository.save(announcement); // to get an ID
//                                                                        // from Mongo
//
//        for (UserData userData : allUsers) {
//            if (!announcement.isPublic && !userData.hasAnyGroups(announcement.groups)) {
//                continue;
//            }
//            if (userData.shouldSendUpdates() && userData.isEnabled() && userData.isAccountNonExpired()) {
//                String usernameAndEmail = String.format("%s <%s>", userData.getUsername(), userData.getEmail());
//                logger.info(String.format("Sending system announcement '%s' to '%s'...", announcement.subject,
//                        usernameAndEmail));
//                sendFromMainEmailAddress(userData.getEmail(), announcement.subject, announcement.body);
//                announcement.sentToUsers.add(usernameAndEmail);
//                announcement.lastSent = DateTime.now();
//                systemAnnouncementRepository.save(announcement);
//            }
//        }
//
//        return announcement;
//    }
//
//    public List<SystemAnnouncement> getRecentSystemAnnouncements() {
//        DateTime earliestValidDate = DateTime.now().plusDays(-1 * MAX_DAYS_FOR_RECENT_ANNOUNCEMENT_RESULTS);
//        List<SystemAnnouncement> allAnnouncements = systemAnnouncementRepository.findAll();
//
//        List<SystemAnnouncement> result = new ArrayList<>();
//        for (SystemAnnouncement announcement : allAnnouncements) {
//            if (announcement.lastSent.isAfter(earliestValidDate) && currentUserCanSeeAnnouncement(announcement)) {
//                result.add(announcement);
//            }
//        }
//
//        return result;
//    }
//
//    private boolean currentUserCanSeeAnnouncement(SystemAnnouncement announcement) {
//        UserData currentUser = securityHelper.getLoggedInUserData();
//        return currentUser.isAdmin() || announcement.groups.isEmpty() || currentUser.hasAnyGroups(announcement.groups);
//    }
//
//    public SystemAnnouncement getAnnouncement(String id) {
//        SystemAnnouncement announcement = systemAnnouncementRepository.findOne(id);
//        if (currentUserCanSeeAnnouncement(announcement)) {
//            return announcement;
//        }
//        return null;
//    }

    private void send(String from, String to, String subject, String msg, File... attachments)
            throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        message.setFrom(new InternetAddress(from));
        message.setRecipients(RecipientType.TO, to);
        message.setSubject(subject);
        message.setText(msg);
        mailSender.send(message);
    }

    public void sendFromMainEmailAddress(String to, String subject, String msg) {
        send(emailConfig.replyableGeneralEmailAddress(), to, subject, msg);
    }

    private void send(String from, String to, String subject, String msg) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(msg);
        mailSender.send(message);
    }
}
