// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.management;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPException;
import org.integratedmodelling.community.beans.Role;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.configuration.CoreApplicationConfig;
import org.integratedmodelling.community.configuration.WebSecurityConfig;
import org.integratedmodelling.community.persistence.TokenRepository;
import org.integratedmodelling.community.security.KLabUserDetailsManager;
import org.integratedmodelling.community.security.SecurityHelper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.verhas.licensor.License;

@Component
public class LicenseManager extends Manager {

    public interface Fields {

        public static final String USER = "user";

        public static final String SERVER = "primary.server";

        public static final String KEY = "key";

        public static final String FIRSTNAME = "firstname";

        public static final String LASTNAME = "lastname";

        public static final String INITIALS = "initials";

        public static final String AFFILIATION = "affiliation";

        public static final String EMAIL = "email";

        public static final String REALNAME = "realname";

        public static final String ROLES = "roles";

        public static final String GROUPS = "groups";

        public static final String APPLICATIONS = "applications";

        public static final String EXPIRY = "expiry";

        public static final String PRIMARY_SERVER = "primary.server";

        // public static final String PHONE = "phone";
        // public static final String ADDRESS = "address";
        // public static final String JOBTITLE = "jobtitle";
        // public static final String COMMENTS = "comments";
        // public static final String MODULES = "modules";
    }

    private static final int CERT_FILE_TTL_DAYS = 365;

    @Autowired
    private KLabUserDetailsManager userDetailsManager;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private WebSecurityConfig webSecurityConfig;

    @Autowired
    private SecurityHelper securityHelper;

    @Autowired
    CoreApplicationConfig coreApplicationConfig;

    public byte[] generateCertFile()
            throws IOException, PGPException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

        License license = new License();
        license.setLicense(getLicensePropertiesString(UUID.randomUUID().toString()));
        license.loadKey(coreApplicationConfig.getKeyringFile(), coreApplicationConfig.getKeyringUserId());
        String encodedLicenseString = license.encodeLicense(coreApplicationConfig.getKeyringPassphrase());

        return encodedLicenseString.getBytes("utf-8");
    }

    private String getLicensePropertiesString(String licenseKey) throws IOException {
        Properties properties = new Properties();
        UserData userData = securityHelper.getLoggedInUserData();
        properties.setProperty(Fields.USER, userData.getUsername());
        properties.setProperty(Fields.AFFILIATION,
                userData.getAffiliation() == null ? "UNDEFINED" : userData.getAffiliation());
        properties.setProperty(Fields.EMAIL, userData.getEmail());
        properties.setProperty(Fields.REALNAME, userData.getFirstName() + " " + userData.getLastName());

        properties.setProperty(Fields.ROLES, sortAndSerializeRoles(userData.getAuthorities()));
        properties.setProperty(Fields.GROUPS, sortAndSerialize(userData.getGroups()));
        properties.setProperty(Fields.APPLICATIONS, sortAndSerialize(userData.getApplications()));
        properties.setProperty(Fields.PRIMARY_SERVER, coreApplicationConfig.getEngineUrl().toExternalForm());

        properties.setProperty(Fields.KEY, licenseKey);
        properties.setProperty(Fields.EXPIRY, DateTime.now().plusDays(CERT_FILE_TTL_DAYS).toString());

        Writer stringWriter = new StringWriter();
        String generatedBy = String.format("Generated by %s on ", coreApplicationConfig.getSiteName()) + new Date();
        properties.store(stringWriter, generatedBy);

        return stringWriter.toString();
    }

    private String sortAndSerializeRoles(Collection<Role> roles) {
        List<String> roleStrings = new ArrayList<>(roles.size());
        for (Role r : roles) {
            roleStrings.add(r.getAuthority());
        }
        return sortAndSerialize(roleStrings);
    }

    private String sortAndSerialize(Collection<String> input) {
        TreeSet<String> sorted = new TreeSet<>(input);
        return StringUtils.join(sorted, ',');
    }

    public Properties readCertFileContent(String certFileContent) throws IOException, PGPException {
        License license = new License();
        license.loadKeyRing(coreApplicationConfig.getKeyringPublicKeyFile(), coreApplicationConfig.getKeyringPublicKeyDigest());
        license.setLicenseEncoded(certFileContent);
        String propertiesString = license.getLicenseString();
        Properties result = new Properties();
        result.load(new StringReader(propertiesString));
        return result;
    }
}
