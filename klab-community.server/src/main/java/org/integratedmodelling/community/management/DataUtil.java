// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.management;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.integratedmodelling.community.beans.ProfileResource;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.exceptions.ValidationException;
import org.integratedmodelling.community.resource.validation.Constraints;
import org.springframework.stereotype.Component;

/**
 * cleanse() methods make allowable *changes* in data objects for the
 * sake of data consistency/cleanliness. These are different than *validation*
 * because they will not reject the data; fields are updated in-line
 * and these methods should never fail or return null.
 */
@Component
public class DataUtil {

    private static final String[] STRIP_USERNAME_PATTERNS = { "\\s" };

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * simple wrapper for annotation-based Hibernate validation. All enforced rules
     * are contained in annotations on the resource object being validated.
     */
    public <T> void validate(T resource) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(resource);
        if (constraintViolations.size() > 0) {
            throw new ValidationException(constraintViolations);
        }
    }

    public void cleanse(UserData userData) {
        if (userData != null) {
            userData.setUsername(cleanseUsername(userData.getUsername()));
            userData.setEmail(cleanseEmail(userData.getEmail()));
        }
    }

    public void cleanse(ProfileResource profileResource) {
        if (profileResource != null) {
            profileResource.username = cleanseUsername(profileResource.username);
            profileResource.email = cleanseEmail(profileResource.email);
        }
    }

//    public void cleanse(AuthenticationRequestResource authenticationResource) {
//        if (authenticationResource != null) {
//            authenticationResource.username = cleanseUsername(authenticationResource.username);
//        }
//    }
//
//    public void cleanse(PasswordChangeResource passwordChangeResource) {
//        if (passwordChangeResource != null) {
//            passwordChangeResource.username = cleanseUsername(passwordChangeResource.username);
//        }
//    }
//
//    public void cleanse(PasswordResetResource resetInfo) {
//        if (resetInfo != null) {
//            resetInfo.username = cleanseUsername(resetInfo.username);
//            resetInfo.email = cleanseEmail(resetInfo.email);
//        }
//    }
//
//    public void cleanse(DataSource dataSource) {
//        if (dataSource != null) {
//            dataSource.creator = cleanseUsername(dataSource.creator);
//            dataSource.name = cleanseDataSourceName(dataSource.name);
//
//            // this helps with the get-by-urn @Query parameters
//            dataSource.serverId = StringUtils.defaultIfEmpty(dataSource.serverId, null);
//            dataSource.continentCode = StringUtils.defaultIfEmpty(dataSource.continentCode, Continent.GLOBAL);
//            dataSource.countryCode = StringUtils.defaultIfEmpty(dataSource.countryCode, null);
//            dataSource.regionCode = StringUtils.defaultIfEmpty(dataSource.regionCode, null);
//            dataSource.theme = StringUtils.defaultIfEmpty(dataSource.theme, null);
//
//            // general defaults
//            if (dataSource.createdDate == null) {
//                dataSource.createdDate = DateTime.now();
//            }
//        }
//    }

    public String cleanseUsername(String username) {
        if (username == null) {
            return null;
        }
        for (String regex : STRIP_USERNAME_PATTERNS) {
            username = username.replaceAll(regex, "");
        }
        return username.trim().toLowerCase();
    }

    public String cleanseEmail(String email) {
        if (email == null) {
            return null;
        }
        return email.trim().toLowerCase();
    }

    public String cleanseDataSourceName(String dataSourceName) {
        if (dataSourceName == null) {
            return null;
        }
        return dataSourceName.trim().toLowerCase();
    }

    public String cleanseFilename(String filename) {
        if (filename == null) {
            return null;
        }
        return filename.trim().toLowerCase().replaceAll(Constraints.FILENAME_ILLEGAL_CHARACTERS, "_");
    }

//    public void enforceUpdateRules(DataSource dataSource, DataSource existingDataSource, String usernameIfAdmin) {
//        // file uploads or other requests may have inserted history items, so keep them around.
//        dataSource.mergeModificationHistory(existingDataSource.getModificationHistory());
//
//        // this only ever gets set by internal workings
//        dataSource.isDB = existingDataSource.isDB;
//
//        if (usernameIfAdmin != null) {
//            if (!existingDataSource.creator.equals(dataSource.creator)) {
//                dataSource.logEvent(dataSource.creator, Type.updatedMetadata,
//                        String.format("Admin user '%s' changed creator from '%s' to '%s'.", usernameIfAdmin,
//                                existingDataSource.creator, dataSource.creator));
//            }
//        } else {
//            // don't let normal users change these fields
//            // (but admins get to because Orphan Recovery requires it)
//            dataSource.creator = existingDataSource.creator;
//            dataSource.createdDate = existingDataSource.createdDate;
//
//            Set<String> filePaths = dataSource.getFilePaths();
//            filePaths.retainAll(existingDataSource.getFilePaths());
//            dataSource.setFilePaths(filePaths);
//        }
//    }
}
