package org.integratedmodelling.community.rest;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.common.auth.UserAuthorizationProvider;
import org.integratedmodelling.common.beans.requests.AuthorizationRequest;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.common.client.RestTemplateHelper;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Simply replicates the authorization endpoints, sending to whatever
 * authorization provider this server uses. For now this allows us to decouple
 * the collaboration server from the certificates; later we can have the
 * collaboration server be a kLAB node and provide direct authentication.
 * 
 * Authentication in the engine is only indirect, although the controller just
 * uses whatever provider is installed.
 * 
 * @author ferdinando.villa
 *
 */
@RestController
public class AuthorizationController /* implements InitializingBean */ {

	@Autowired
	UserAuthorizationProvider authorizationProvider;

	List<String> localCertificates;

	/**
	 * Authorize using username/password. Delegate to authorization provider for
	 * the actual authentication.
	 * 
	 * @param request
	 * @return the response bean
	 * @throws KlabAuthorizationException
	 */
	@CrossOrigin
	@RequestMapping(value = API.AUTHENTICATION, method = RequestMethod.POST)
	public @ResponseBody AuthorizationResponse authorizeFromUsername(@RequestBody AuthorizationRequest request)
			throws KlabAuthorizationException {
		return authorizationProvider.authenticateUser(request.getUsername(), request.getPassword());
	}

	/**
	 * Authorize through cert file delegating to authorization provider. If
	 * authentication fails, check out any certificates in
	 * 
	 * <pre>
	 * data
	 * </pre>
	 * 
	 * /authorization subdir and if we have a match, make a user anyway and
	 * assign an internally valid token.
	 * 
	 * @param certificate
	 * @return a response bean.
	 * @throws KlabAuthorizationException
	 */
	@RequestMapping(value = API.AUTHENTICATION_CERT_FILE, method = RequestMethod.POST, consumes = "text/plain")
	public @ResponseBody AuthorizationResponse authorizeFromCertificate(@RequestBody String certificate)
			throws KlabAuthorizationException {
		return authorizationProvider.authenticateUser(certificate);
	}

	// @Override
	// public void afterPropertiesSet() throws Exception {
	// if (!KLAB.CONFIG.isOffline() && authorizationProvider instanceof
	// IndirectAuthorizationProvider) {
	// ((IndirectAuthorizationProvider) authorizationProvider)
	// .setAuthorizationEndpoints(((IEngineNetwork)
	// KLAB.ENGINE.getNetwork()).getPrimaryNode()
	// .getUrl());
	// }
	// }

	@RequestMapping(value = { API.GET_RESOURCE_INFO }, method = RequestMethod.GET)
	@ResponseBody
	public Object getResourceInfo(@PathVariable String urn) throws KlabException {

		RestTemplateHelper template = RestTemplateHelper.newTemplate();
		IServer nd = KLAB.ENGINE.getNetwork().getNode(urn.substring(0, urn.indexOf(":")));
		if (nd != null) {
			return template.with(ResourceFactory.getUrnAuthorization(urn))
					.get((nd.getUrl() + API.GET_RESOURCE_INFO).replace("{urn}", urn) + ".json", Map.class);
		}
		return null;
	}

}
