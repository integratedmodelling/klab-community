// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.api;

public interface Endpoints {

    // authentication & profile
    public static final String AUTHENTICATION = "/authentication";

    public static final String AUTHENTICATION_CERT_FILE = "/authentication/cert-file";

    public static final String CERT_FILE = "/cert-file";

    public static final String CLICKBACK = "/clickback/{tokenString}";

    public static final String ANNOUNCEMENTS = "/announcements";

    public static final String ANNOUNCEMENT_BY_ID = "/announcement/{id}";

    // the clickback version needs to be different because it authenticates via clickback token, not login
    // token.
    public static final String PASSWORD_VIA_CLICKBACK = "/password-clickback";

    public static final String PASSWORD = "/password";

    public static final String PROFILE = "/profile";

    // uploads
    public static final String UUID = "/uuid";

    public static final String UPLOAD_METADATA = "/metadata/{uuid}";

    public static final String UPLOAD = "/upload/{uuid}";

    // data access
    public static final String DATASOURCES = "/datasources";

    public static final String DATASOURCE_BY_ID = "/datasource/{id}";

    public static final String DATASOURCE_FILE_BY_ID = "/datasource/{id}/{filename}";

    public static final String DATASOURCE_BY_URN = "/datasource-urn/{urn}";

    // admin
    public static final String ADMIN_ANNOUNCEMENTS = "/admin/announcements";

    public static final String ADMIN_DATASOURCES = "/admin/datasources";

    public static final String ADMIN_PROFILE = "/admin/profile";

    public static final String ADMIN_PROFILE_BY_USERNAME = "/admin/profile/{username}";

    public static final String ADMIN_TOKENS = "/admin/tokens";

    public static final String ADMIN_ORPHANS = "/admin/orphans";

    public static final String ADMIN_ORPHANS_POSTGIS = "/admin/orphans-postgis";

    public static final String ADMIN_ORPHANS_POSTGIS_BY_TABLENAME = "/admin/orphans-postgis/{tableName}";

    public static final String ADMIN_DEAD_LINKS = "/admin/deadlinks";

    public static final String ADMIN_ORPHAN_BY_ID = "/admin/orphan/{id}";

    // engine
    public static final String ENGINE_ANNOUNCEMENTS = "/engine/announcements";

    public static final String ENGINE_ANNOUNCEMENT_BY_ID = "/engine/announcement/{id}";

    public static final String ENGINE_GET_ASSET = "/engine/get-asset";

    public static final String ENGINE_DATASOURCE_BY_URN = "/engine/datasource/{urn}";

    public static final String ENGINE_PROFILE = "/engine/profile";

    // testing
    public static final String STATUS = "/status";

    public static final String GLOBAL_CONFIG = "/global-config";

    public static final String AUTH_TEST_UNSECURED = "/auth_test_unsecured";

    public static final String AUTH_TEST_SECURED = "/auth_test_secured";

    public static final String ENGINE_TEST_SECURED = "/engine/auth_test_secured";

}
