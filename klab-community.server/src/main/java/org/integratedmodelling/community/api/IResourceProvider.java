package org.integratedmodelling.community.api;

/**
 * General interface implemented by any resource provider type - e.g. a data server or
 * repository. Implementations are provided through plug-in jars and bring their own
 * configuration and service interface with them.
 * 
 * @author ferdinando.villa
 *
 */
public interface IResourceProvider {

}
