// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.exceptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST)
@SuppressWarnings("rawtypes")
public final class ValidationException extends KLabException {

    private static final long serialVersionUID = -3921597449342910649L;

    // this is actually Set<ConstraintViolation<? extends Object>>, and
    // will also work as Set<String>, but those would create ugly calling code
    private Set constraintViolations;

    public ValidationException(Collection constraintViolations) {
        this(constraintViolations, null);
    }

    @SuppressWarnings("unchecked")
    public ValidationException(Collection constraintViolations, Throwable t) {
        super("Invalid field(s): " + describeConstraintViolations(constraintViolations), t);

        this.constraintViolations = new HashSet(constraintViolations);
    }

    private static String describeConstraintViolations(Collection constraintViolations) {
        List<String> descriptions = new ArrayList<>(constraintViolations.size());
        for (Object v : constraintViolations) {
            String msg = v.toString();
            try {
                ConstraintViolation violation = (ConstraintViolation) v;
                msg = String.format("%s %s", violation.getPropertyPath().toString(), violation.getMessage());
            } catch (Throwable t) {
                // probably the wrong type
            }
            descriptions.add(msg);
        }
        return StringUtils.join(descriptions, "; ");
    }

    public Set getConstraintValidations() {
        return constraintViolations;
    }

    @SuppressWarnings("unchecked")
    public void addVoilation(Object v) {
        constraintViolations.add(v);
    }
}
