// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.integratedmodelling.community.KLabLogger;
import org.integratedmodelling.community.beans.tokens.AuthenticationToken;
import org.integratedmodelling.community.configuration.WebSecurityConfig;
import org.integratedmodelling.community.persistence.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class TokenHeaderProcessingFilter implements Filter {

    private final KLabLogger logger = new KLabLogger(getClass());

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Initialized TokenHeaderProcessingFilter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        try {
            String tokenString = ((HttpServletRequest) request)
                    .getHeader(WebSecurityConfig.AUTHENTICATION_TOKEN_HEADER_NAME);

            if (tokenString != null) {
                // check the token against mongo
                AuthenticationToken storedToken = tokenRepository.findByTokenString(tokenString);
                if (storedToken != null && storedToken.isAuthenticated()) {
                    // successful match. token should contain everything the security context needs.
                    SecurityContextHolder.getContext().setAuthentication(storedToken);
                }
            }
        } catch (Throwable e) {
            logger.error("something is weird: ", e);
        } finally {
            // ALWAYS continue processing the filter chain. This is because we don't know
            // whether or not the resource requested is public or private.
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
