// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.security;

import org.integratedmodelling.community.beans.ProfileResource;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.beans.tokens.AuthenticationToken;
import org.integratedmodelling.community.configuration.CoreApplicationConfig;
import org.integratedmodelling.community.management.ProfileManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * TODO not sure if this class should be separate from KLabUserDetailsManager.
 * Its interfaces are simpler, so it's kind of an adaptor, but the methods could easily
 * just be in KLabUserDetailsManager since that's what this thing drives.
 *
 * The one reason why I haven't done this yet is ProfileManager seems to be a higher level
 * of abstraction than UserDetailsManager; merging this class into KLabUserDetailsManager
 * would force ProfileManager to also be merged and this seems like bad (upside-down) style.
 */
@Component
public class SecurityHelper {

    @Autowired
    private KLabUserDetailsManager userDetailsManager;

    @Autowired
    private ProfileManager profileManager;

    @Autowired
    private CoreApplicationConfig coreApplicationConfig;

    @Autowired
    ObjectMapper objectMapper;

    public String getLoggedInUsername() {
        String result = "-unauthenticated user-";
        try {
            Object principal = getLoggedInAuthentication().getPrincipal();
            result = principal.toString();
        } catch (Throwable e) {
        }
        return result;
    }

    public UserData getLoggedInUserData() {
        String username = getLoggedInUsername();
        UserData user = getUserData(username);
        return user;
    }

    public ProfileResource getLoggedInUserProfile() {
        UserData userData = getLoggedInUserData();
        ProfileResource result = objectMapper.convertValue(userData, ProfileResource.class);
        return result;
    }

    public Authentication getLoggedInAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public boolean isAdministrator() {
        AuthenticationToken token = (AuthenticationToken) getLoggedInAuthentication();
        return token == null ? false : token.isAdministrator();
    }

    public UserData getUserData(String username) {
        UserData user = userDetailsManager.loadUserByUsername(username);
        profileManager.decorate(user);
        return user;
    }
}
