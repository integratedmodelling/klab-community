// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.security;

import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.community.KLabLogger;
import org.integratedmodelling.community.beans.Role;
import org.integratedmodelling.community.beans.UserData;
import org.integratedmodelling.community.beans.UserData.AccountStatus;
import org.integratedmodelling.community.exceptions.BadRequestException;
import org.integratedmodelling.community.exceptions.UserEmailExistsException;
import org.integratedmodelling.community.exceptions.UserExistsException;
import org.integratedmodelling.community.management.DataUtil;
import org.integratedmodelling.community.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;


/**
 * A wrapper object which can handle merging a "Spring user" with user data from MongoDB.
 * In a production environment, the "Spring user" comes from LDAP, and in a test environment,
 * it comes from the in-memory mock.
 *
 * In both scenarios, MongoDB holds most of the information we care about.
 *
 * The data that is stored on the "Spring user" is:
 * - username (also in mongo)
 * - password
 * - authorities (roles)
 *
 * NOTE: @Component is not valid here because it needs to be on the concrete class
 *
 * @author luke
 */
public abstract class KLabUserDetailsManager implements UserDetailsManager {

    protected final KLabLogger logger = new KLabLogger(getClass());

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected SecurityHelper securityHelper;

    @Autowired
    DataUtil dataUtil;

    protected abstract UserDetailsManager getWrappedObject();

    /**
     * NOTE: this is the main UserDetailsService method
     *
     * @return a fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no GrantedAuthority
     */
    @Override
    public UserData loadUserByUsername(String username) throws UsernameNotFoundException {
        username = dataUtil.cleanseUsername(username);
        UserData user = getUserFromMongo(username);

        // load authorities from LDAP (overwrite MongoDB authorities during read operations)
        UserDetails springUser = null;
        try {
            springUser = getSpringUser(username);
        } catch (Exception e) {
            logger.warn(String.format("User '%s' exists in Mongo but not in LDAP.", username));
        }

        if (springUser != null) {
            Set<Role> roles = new HashSet<>();
            for (GrantedAuthority authority : springUser.getAuthorities()) {
                try {
                    // TODO this is the hard-coded part of the Roles
                    roles.add(Role.valueOf(authority.getAuthority()));
                } catch (ClassCastException e) {
                    logger.error(String.format("Couldn't convert LDAP Authority '%s' to a Role object.", authority));
                    // TODO create a custom-role object? Authority will be in the format ROLE_*
                    // roles.add(Role.customRole(authority.getAuthority()));
                }
            }
            user.setRoles(roles);
            user.setPasswordHash(springUser.getPassword());
        }
        return user;
    }

    private UserDetails getSpringUser(String username) {
        return getWrappedObject().loadUserByUsername(username);
    }

    public boolean springUserExists(String username) {
        username = dataUtil.cleanseUsername(username);
        UserDetails user = null;
        try {
            user = getSpringUser(username);
        } catch (AuthenticationException e) {
        }
        return user != null;
    }

    private UserData getUserFromMongo(String username) {
        username = dataUtil.cleanseUsername(username);
        UserData user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return user;
    }

    public UserData createMongoUser(UserData userData) throws UserExistsException, UserEmailExistsException {
        return createMongoUser(userData, AccountStatus.pendingActivation);
    }

    public UserData createMongoUser(UserData userData, AccountStatus accountStatus)
            throws UserExistsException, UserEmailExistsException {
        dataUtil.cleanse(userData);

        if (userExists(userData.getUsername())) {
            throw new UserExistsException(userData.getUsername());
        }
        if (userRepository.findByEmail(userData.getEmail()) != null) {
            throw new UserEmailExistsException(userData.getEmail());
        }

        userData.setAccountStatus(accountStatus);
        userData = userRepository.save(userData);

        // NOTE: don't create an LDAP record until user is activated with activateUser()

        return userData;
    }

    public void createSpringUser(UserData user) {
        createUser(user);
    }

    /**
     * deprecated to avoid naming confusion.
     * callers should distinguish between createMongoUser() and createSpringUser().
     */
    @Override
    @Deprecated
    public void createUser(UserDetails userDetails) {
        if (userDetails instanceof UserData) {
            dataUtil.cleanse((UserData) userDetails);
        }
        getWrappedObject().createUser(userDetails);
    }

    /**
     * called when a user clicks on an email link, proving that they own that email address.
     */
    public void activateUser(String username) {
        UserData userData = loadUserByUsername(username);
        if (AccountStatus.active.equals(userData.getAccountStatus())) {
            // if already active, return silently (don't want a repeat-click to overwrite a changed password)
        } else if (AccountStatus.pendingActivation.equals(userData.getAccountStatus())) {
            // update the account status and create the LDAP user
            userData.setAccountStatus(AccountStatus.active);
            userRepository.save(userData);
        } else {
            throw new BadRequestException("Account was not in a valid state for activation.");
        }
    }

    /**
     * the canonical profile update (dao) method
     */
    public void updateUser(UserData userData) {
        dataUtil.cleanse(userData);
        UserData mongoUser = getUserFromMongo(userData.getUsername());
        userData.setId(mongoUser.getId());
        userRepository.save(userData);
        try {
            updateSpringUser(userData);
        } catch (Throwable e) {
            // return success if only LDAP fails - sometimes users aren't activated before updates happen
            logger.error(String.format("Mongo update succeeded but Spring/LDAP user update failed for '%s'.",
                    userData.getUsername()), e);
        }
    }

    @Override
    public void updateUser(UserDetails userDetails) {
        getWrappedObject().updateUser(userDetails);
    }

    public void updateSpringUser(UserData userData) {
        updateUser((UserDetails) userData);
    }

    @Override
    public void deleteUser(String username) {
        username = dataUtil.cleanseUsername(username);
        UserData user = getUserFromMongo(username);
        user.setAccountStatus(AccountStatus.deleted);
        userRepository.save(user);
        getWrappedObject().deleteUser(username);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) throws AuthenticationException {
        getWrappedObject().changePassword(oldPassword, newPassword);
    }

    public void changePasswordForUser(String username, String password) {
        username = dataUtil.cleanseUsername(username);
        Authentication realAuthentication = SecurityContextHolder.getContext().getAuthentication();
        // NOTE: this is not created with createAuthenticationToken() because it's not saved to the DB
        Authentication temporaryAuthentication = new UsernamePasswordAuthenticationToken(username, Role.TEMPORARY);

        // BEGIN - SENSITIVE! need to temporarily "become" the given user to change its password
        SecurityContextHolder.getContext().setAuthentication(temporaryAuthentication);
        changePassword(null, password);
        SecurityContextHolder.getContext().setAuthentication(realAuthentication); // back to normal
        // END
    }

    @Override
    public boolean userExists(String username) {
        username = dataUtil.cleanseUsername(username);
        // NOTE: we silently add users to LDAP when activating accounts through clickback tokens,
        // so we don't actually check LDAP to determine whether a user exists already. If they exist
        // in Mongo, then they exist. If they don't exist in Mongo, then they don't exist.
        UserData user = userRepository.findByUsername(username);
        return user != null;
    }
}
