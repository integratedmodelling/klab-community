// - BEGIN LICENSE: -4910638947056778875 -
//
// Copyright (C) 2007-2016 by:
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - J. Luke Scott <luke@cron.works>
// - integratedmodelling.org
// - any other authors listed in @author annotations
//
// All rights reserved. This file is part of the k.LAB software suite, meant to
// enable modular, collaborative, integrated development of interoperable data
// and model components. For details, see http://integratedmodelling.org.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.community.fixture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.json.JacksonJsonParser;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A class for reading JSON fixture objects and making them available to unit tests.
 *
 * It is assumed that each JSON file (.js) includes a top-level object
 * which contains key:value entries, each value being an individual data fixture.
 *
 * NOTE: the '_id' field in Mongo is represented as 'id' in fixtures and POJOs.
 *
 * For instance:
 * {
 *      "luke": {
 *          "id": 1,
 *          "city": "Seattle",
 *          "country": "USA"
 *      },
 *      "ferdinando": {
 *          "id": 2,
 *          "city": "Bilbao",
 *          "country": "Spain"
 *      }
 * }
 *
 * @author luke
 */
public abstract class FixtureCollection<T> {

    private static final String FIXTURE_RELATIVE_PATH = "fixtures";

    private Log log = LogFactory.getLog(getClass());

    JacksonJsonParser jsonParser = new JacksonJsonParser();

    ObjectMapper objectMapper;

    private Map<String, T> jsonData;

    private String jsonString;

    private String filename;

    public FixtureCollection(String filename, ObjectMapper objectMapper) {
        this(filename, objectMapper, false);
    }

    @SuppressWarnings("unchecked")
    public FixtureCollection(String filename, ObjectMapper objectMapper, boolean supressFileNotFound) {
        this.filename = filename;
        this.objectMapper = objectMapper;

        StringBuilder result = new StringBuilder();
        try {
            InputStream stream = getFixtureInputStreamForFilename(filename);
            if (stream == null) {
                throw new FileNotFoundException(filename);
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            reader.close();
            jsonString = result.toString();
            jsonData = (Map<String, T>) jsonParser.parseMap(jsonString);
        } catch (FileNotFoundException e) {
            if (!supressFileNotFound) {
                log.error(String.format(
                        "Could not open fixture file '%s' - maybe you forgot to add src/test/resources to your classpath?",
                        filename), e);
                log.error("PATH=" + System.getProperty("PATH"));
            }
        } catch (IOException e) {
            log.error("Got IOException when reading fixture file: " + filename, e);
        }
    }

    private InputStream getFixtureInputStreamForFilename(String filename) throws FileNotFoundException {
        InputStream result;
        if (filename.startsWith("/")) {
            // absolute path
            result = new FileInputStream(new File(filename));
        } else {
            // relative path - look for it in the classpath
            filename = FIXTURE_RELATIVE_PATH + File.separatorChar + filename;
            result = getClass().getClassLoader().getResourceAsStream(filename);
        }
        return result;
    }

    public String getJsonString(String key) {
        T node = get(key);
        StringWriter writer = new StringWriter();
        String result = null;
        try {
            objectMapper.writeValue(writer, node);
            result = writer.toString();
        } catch (IOException e) {
            log.error(String.format("Failed reading JSON string from file %s,  node %s", filename, key), e);
        }
        return result;
    }

    public T get(String key) {
        T result = objectMapper.convertValue(jsonData.get(key), getContainedClass());
        return result;
    }

    public Set<String> keySet() {
        return jsonData.keySet();
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        if (jsonData == null) {
            return new ArrayList<>(0);
        }

        List<T> result = new ArrayList<>(jsonData.size());
        Class<? extends T> targetClass = getContainedClass();
        for (T item : jsonData.values()) {
            try {
                LinkedHashMap<String, Object> linkedHashMap = (LinkedHashMap<String, Object>) item;
                String targetClassName = (String) linkedHashMap.get("_class");
                targetClass = (Class<T>) Class.forName(targetClassName);
                linkedHashMap.remove("_class");
            } catch (Exception e) {
                // _class probably wasn't set
            }
            // without the conversion, records would save as _class:java.util.LinkedHashMap
            result.add(objectMapper.convertValue(item, targetClass));
        }
        return result;
    }

    public long count() {
        return jsonData.size();
    }

    @Override
    public String toString() {
        return jsonData.toString();
    }

    protected abstract Class<? extends T> getContainedClass();
}
